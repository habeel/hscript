
using System;

namespace hscript.VirtualMachine
{
	public enum HSVariableType
	{
		VOID,
		NUMBER,
		STRING
	}
	
	public class HSVariable
	{
		public String Name { get; private set; }
		
		public HSVariableType Type { get; private set; }
		
		public int NumberValue { get; private set; }
		
		public string StringValue { get; private set; }
		
		public HSVariable (string name)
		{
			Name = name;
			NumberValue = 0;
			StringValue = String.Empty;
			Type = HSVariableType.VOID;
		}
		
		public void Set(int num)
		{
			NumberValue = num;
			Type = HSVariableType.NUMBER;
		}
		
		public void Set(string str)
		{
			StringValue = str;
			Type = HSVariableType.STRING;
		}
	}
}
