
using System;
using System.Collections.Generic;

namespace hscript.VirtualMachine
{
	public enum HSScopeType
	{
		GLOBAL,
		FUNCTION,
		WHILE,
		IF
	}
	
	public class HSScope
	{
		public HSScope Previous;
		
		public HSScopeType Type;
		
		public HSStorage Storage;
		
		public HSScope (HSScopeType type)
		{
			Type = type;
			Previous = null;
			Storage = new HSStorage();
		}
		
		public HSVariable GetVariable(string name)
		{
			if(Storage.GetVariable(name) != null)
			{
				return Storage.GetVariable(name);
			}
			else
			{
				if(Previous == null)
				{
					return null;
				}
				else
				{
					return Previous.GetVariable(name);
				}
			}
		}
	}
}
