
using System;

namespace hscript.VirtualMachine
{
	public class HSFrame
	{
		public int SavedPointer;
		public HSScope CurrentScope;
		
		public HSFrame (HSScope gscope)
		{
			SavedPointer = 0;
			// Create a function scope and set global scope as the previous scope!
			CurrentScope = new HSScope(HSScopeType.FUNCTION);
			CurrentScope.Previous = gscope;
		}
		
		// Called when a function returns
		public void Destroy()
		{
			while(CurrentScope.Type != HSScopeType.GLOBAL)
			{
				EndScope();
			}
		}
		
		public void BeginScope(HSScopeType type)
		{
			HSScope newscope = new HSScope(type);
			HSScope prevscope = CurrentScope;
			newscope.Previous = prevscope;
			
			CurrentScope = newscope;
		}
		
		public void EndScope()
		{
			if(CurrentScope.Type == HSScopeType.GLOBAL)
			{
				// TODO: Log this message
				Console.WriteLine("Trying to exit the global scope! WEIRD ERROR");
				return;
			}
			
			HSScope newscope = CurrentScope.Previous;
			CurrentScope.Storage.Destroy();
			
			CurrentScope = newscope;
		}
	}
}
