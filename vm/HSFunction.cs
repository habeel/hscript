
using System;

namespace hscript.VirtualMachine
{
	public class HSFunction
	{
		public String Name { get; private set; }
		public int Location { get; private set; }
		
		public HSFunction (string name, int loc)
		{
			Name = name;
			Location = loc;
		}
	}
}
