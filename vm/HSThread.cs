
using System;
using System.IO;
using System.Collections.Generic;

namespace hscript.VirtualMachine
{
	public enum HSThreadState
	{
		RUNNING,
		PAUSED,
		SLEEP,
		DEAD
	}
	
	public class HSThread
	{
		public int ID { get; private set; }
		private HSVirtualMachine machine;
		public HSContext Context;
		public Stack<HSFrame> FrameStack;
		
		public HSFrame CurrentFrame;
		public HSScope GlobalScope;
		
		public HSThreadState State;
		
		public DateTime AwakeTime;
		
		public HSThread (HSVirtualMachine hvm, int id, HSContext ctx, HSScope gscope)
		{
			ID = id;
			machine = hvm;
			Context = ctx;
			FrameStack = new Stack<HSFrame>();
			GlobalScope = gscope;
			
			CurrentFrame = new HSFrame(gscope);
			FrameStack.Push(CurrentFrame);
			
			State = HSThreadState.PAUSED;
			AwakeTime = DateTime.Now;
		}
		
		public void ExecuteOne()
		{
			if(State == HSThreadState.DEAD)
			{
				return;
			}
			
			string line = string.Empty;
			bool eof = false;
			while(true)
			{
				char c = Context.ReadOneChar();
				if((c == (char)0) || (c == '\n'))
				{
					if(c == (char)0) { eof = true; }
					break;
				}
				else
				{
					line += c;
				}
			}
			
			if(line.Length > 0)
			{
				machine.Execute(this, line);
				
				if(FrameStack.Count == 0)
				{
					State = HSThreadState.DEAD;
				}
				else if(eof)
				{
					State = HSThreadState.DEAD;
				}
			}
		}
	}
}
