
using System;
using System.Collections.Generic;

namespace hscript.VirtualMachine
{
	public class HSContext
	{
		public string Code;
		public int Pointer;
		public Stack<HSValue> Stack;
		
		public HSContext (string code, int start)
		{
			Pointer = start;
			Code = code;
			Stack = new Stack<HSValue>();
		}
		
		public char ReadOneChar()
		{
			if(Pointer >= Code.Length)
			{
				return (char)0;
			}
			else
			{
				char ret = Code[Pointer];
				Pointer++;
				return ret;
			}
		}
	}
}
