
using System;

namespace hscript.VirtualMachine
{
	public enum HSValueType
	{
		VOID,
		NUMBER,
		STRING,
		VARIABLE
	}
	
	public class HSValue
	{
		public HSValueType Type { get; private set; } 
		public int NumberValue { get; private set; }
		public string StringValue { get; private set; }
		public string VariableValue { get; private set; }
		
		public HSValue ()
		{
			Type = HSValueType.VOID;
			NumberValue = 0;
			StringValue = String.Empty;
			VariableValue = String.Empty;
		}
		
		public void SetNumber(int num)
		{
			NumberValue = num;
			Type = HSValueType.NUMBER;
		}
		
		public void SetString(string str)
		{
			StringValue = str;
			Type = HSValueType.STRING;
		}
		
		public void SetVariable(string variable)
		{
			VariableValue = variable;
			Type = HSValueType.VARIABLE;
		}
	}
}
