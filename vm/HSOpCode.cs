
using System;

namespace hscript.VirtualMachine
{
	public enum HSOpCodeType
	{
		or,
		and,
		eq,
		neq,
		mt,
		mteq,
		lt,
		lteq,
		add,
		sub,
		mul,
		div,
		neg,
		push,
		jmpif,
		jmp,
		sys,
		call,
		ret,
		var,
		pop
	}
}
