
using System;
using System.Collections.Generic;

namespace hscript.VirtualMachine
{
	public class HSStorage
	{
		private Dictionary<string, HSFunction> functions;
		
		private Dictionary<string, HSLabel> labels;
		
		private Dictionary<string, HSVariable> variables;
		
		public HSStorage ()
		{
			functions = new Dictionary<string, HSFunction>();
			variables = new Dictionary<string, HSVariable>();
			labels = new Dictionary<string, HSLabel>();
		}
		
		public void Destroy()
		{
			variables.Clear();
			functions.Clear();
			labels.Clear();
		}
		
		public void AddVariable(string name)
		{
			if(!variables.ContainsKey(name))
			{
				variables[name] = new HSVariable(name);
			}
		}
		
		public HSVariable GetVariable(string name)
		{
			if(variables.ContainsKey(name))
			{
				return variables[name];
			}
			else
			{
				return null;
			}
		}
		
		public void AddFunction(string name, int loc)
		{
			if(!functions.ContainsKey(name))
			{
				functions[name] = new HSFunction(name, loc);
			}
		}
		
		public HSFunction GetFunction(string name)
		{
			if(functions.ContainsKey(name))
			{
				return functions[name];
			}
			else
			{
				return null;
			}
		}
		
		public void AddLabel(string name, int loc)
		{
			if(!labels.ContainsKey(name))
			{
				labels[name] = new HSLabel(name, loc);
			}
		}
		
		public HSLabel GetLabel(string name)
		{
			if(labels.ContainsKey(name))
			{
				return labels[name];
			}
			else
			{
				return null;
			}
		}
	}
}
