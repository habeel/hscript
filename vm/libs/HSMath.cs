using System;

namespace hscript.VirtualMachine.Library
{
	public class HSMath
	{
		public HSMath ()
		{
		}
		
		public void Register(HSVirtualMachine vm)
		{
			vm.HookSysFunc("Random", this.Random);
			vm.HookSysFunc("Mod", this.Mod);
		}
		
		public void Random(HSVirtualMachine vm, HSThread thread)
		{
			int random = new Random().Next();
			
			HSValue val = new HSValue();
			val.SetNumber(random);
			
			thread.Context.Stack.Push (val);
		}
		
		public void Mod(HSVirtualMachine vm, HSThread thread)
		{
			HSValue v1 = thread.Context.Stack.Pop();
			HSValue v2 = thread.Context.Stack.Pop();
			
			int v1int = vm.getIntValue(thread, v1);
			int v2int = vm.getIntValue(thread, v2);
			
			int ret = v1int % v2int;
			HSValue val = new HSValue();
			val.SetNumber(ret);
			thread.Context.Stack.Push (val);
		}
	}
}

