
using System;

namespace hscript.VirtualMachine.Library
{
	public class HSConsole
	{
		public HSConsole ()
		{
		}
		
		public void Register(HSVirtualMachine vm)
		{
			vm.HookSysFunc("Print", this.Print);
		}
		
		public void Print(HSVirtualMachine vm, HSThread thread)
		{
			HSValue v1 = thread.Context.Stack.Pop();
				
			int v1int = vm.getIntValue(thread, v1);
			string v1str = vm.getStringValue(thread, v1);
			HSValueType vtype = v1.Type;
			
			if(vtype == HSValueType.NUMBER)
			{
				Console.Write(v1int);
			}
			else
			{
				Console.Write(v1str);
			}
		}
	}
}
