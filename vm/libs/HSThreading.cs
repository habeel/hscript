
using System;

namespace hscript.VirtualMachine.Library
{
	public class HSThreading
	{
		public HSThreading ()
		{
		}
		
		public void Register(HSVirtualMachine vm)
		{
			vm.HookSysFunc("Sleep", this.Sleep);
			
			vm.HookSysFunc("ThreadCreate", this.Create);
			vm.HookSysFunc("ThreadResume", this.Resume);
			vm.HookSysFunc("ThreadPause", this.Pause);
			vm.HookSysFunc("ThreadAbort", this.Abort);
		}
		
		public void Sleep(HSVirtualMachine vm, HSThread thread)
		{
			HSValue v1 = thread.Context.Stack.Pop();
				
			int sleepms = vm.getIntValue(thread, v1);
				
			thread.AwakeTime = DateTime.Now.AddMilliseconds((double)sleepms);
			thread.State = HSThreadState.SLEEP;
		}
		
		public void Create(HSVirtualMachine vm, HSThread thread)
		{
			// pop the function name 
			HSValue hval = thread.Context.Stack.Pop();
			
			string fname = "";
			if(hval.Type != HSValueType.VARIABLE)
			{
				if(hval.StringValue.Length > 0)
				{
					fname = hval.StringValue;
				}
				else
				{
					return;
				}
			}
			else
			{
				fname = hval.VariableValue;
			}
			
			HSFunction func = vm.GlobalScope.Storage.GetFunction(fname);
			
			if(func == null) { return; }
			
			HSContext ctx = new HSContext(vm.Code, func.Location);
			
			// all threads are created in paused state!
			int threadid = vm.generateThreadId();
			HSThread nthread = new HSThread(vm, threadid, ctx, vm.GlobalScope);
			nthread.State = HSThreadState.PAUSED;
			
			vm.PendingThreads.Add(threadid, nthread);
			
			HSValue val = new HSValue();
			val.SetNumber(threadid);
			
			// push the thread id
			thread.Context.Stack.Push(val);
		}
		
		public void Resume(HSVirtualMachine vm, HSThread thread)
		{
			HSValue val = thread.Context.Stack.Pop();
			int threadid = vm.getIntValue(thread, val);
			if(threadid == 0) { return; }
			if(vm.Threads.ContainsKey(threadid))
			{
				vm.Threads[threadid].State = HSThreadState.RUNNING;
			}
		}
		
		public void Pause(HSVirtualMachine vm, HSThread thread)
		{
			HSValue val = thread.Context.Stack.Pop();
			int threadid = vm.getIntValue(thread, val);
			if(threadid == 0) { return; }
			if(vm.Threads.ContainsKey(threadid))
			{
				vm.Threads[threadid].State = HSThreadState.PAUSED;
			}
		}
		
		public void Abort(HSVirtualMachine vm, HSThread thread)
		{
			HSValue val = thread.Context.Stack.Pop();
			int threadid = vm.getIntValue(thread, val);
			if(threadid == 0) { return; }
			if(vm.Threads.ContainsKey(threadid))
			{
				vm.Threads[threadid].State = HSThreadState.DEAD;
			}
		}
	}
}
