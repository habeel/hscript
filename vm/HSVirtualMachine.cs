
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace hscript.VirtualMachine
{
	public class HSVirtualMachine
	{
		private string asm;
		private int pointer;	// used to iterate over all the code to execute global statements & record function names
		private Dictionary<int, HSThread> threads;	// keeps track of all threads
		private int threadidGen;	// used to generate unique thread ids
		private HSScope scope;		// global scope
		
		public Dictionary<int, HSThread> Threads
		{
			get { return threads; }
		}
		
		public Dictionary<int, HSThread> PendingThreads { get; private set; }
		
		public HSScope GlobalScope
		{
			get { return scope; }
		}
		
		public String Code
		{
			get { return asm; }
		}
		
		public delegate void SysFunc(HSVirtualMachine vm, HSThread thread);
		
		public Dictionary<string, SysFunc> sysfuncs;
		
		public HSVirtualMachine ()
		{
			asm = "";
			pointer = 0;
			threads = new Dictionary<int, HSThread>();
			threadidGen = 0;
			// the one and only global scope!
			scope = new HSScope(HSScopeType.GLOBAL);
			sysfuncs = new Dictionary<string, HSVirtualMachine.SysFunc>();
			PendingThreads = new Dictionary<int, HSThread>();
		}
		
		public void HookSysFunc(string name, SysFunc func)
		{
			sysfuncs[name] = func;
		}
		
		public void Scan()
		{
			StringReader reader = new StringReader(asm);
			
			List<string> clist = new List<string>();
			while(pointer < asm.Length)
			{
				string line = reader.ReadLine();
				if(line == null)
				{
					return;
				}
				pointer += line.Length + 1;
				
				// if its a func
				if(line.Length >= 2 && line[0] == ':' && line[1] == ':')
				{
					string fname = line.Substring(2);
					scope.Storage.AddFunction(fname, pointer);
					Execute(clist);
					clist.Clear();
				}
				else if(line.Length >= 1 && line[0] == ':')
				{
					string lname = line.Substring(1);
					scope.Storage.AddLabel(lname, pointer);
				}
				else if(line == "ret")
				{
					clist.Clear();
				}
				else
				{
					clist.Add(line);
				}
			}
		}
		
		public void Init()
		{	
			HSContext mcontext = new HSContext(asm, 0);
			int threadid = generateThreadId();
			HSThread mainthread = new HSThread(this, threadid, mcontext, scope);
			
			Scan();
			
			// find the main func
			HSFunction mainfunc = scope.Storage.GetFunction("main");
			// basically if a file doesnt have main function we dont execute it!
			if(mainfunc == null) { return; }
			
			mainthread.Context.Pointer = mainfunc.Location;
			mainthread.State = HSThreadState.RUNNING;
			threads.Add(threadid, mainthread);
		}
		
		public void Run()
		{
			Run(new TimeSpan(0), true);
		}
		
		// run for a certain amount of time
		public void Run(TimeSpan time)
		{
			Run(time, false);
		}
		
		public void Run(TimeSpan time, bool notimeout)
		{
			DateTime lastinternalsleep = DateTime.Now;
			if(threads.Count == 0)
			{
				return;
			}
			
			Random random = new Random();
			
			DateTime endtime = DateTime.Now.Add(time);
			while((DateTime.Now < endtime) || (notimeout))
			{
				if(threads.Count == 0)
				{
					break;
				}
				
				List<int> tdlist = new List<int>();
				foreach(int tid in threads.Keys)
				{
					HSThread thread = threads[tid];
					// execute 1 statement
					if(thread.State == HSThreadState.RUNNING)
					{
						thread.ExecuteOne();
						if ((DateTime.Now - lastinternalsleep).TotalMilliseconds > 10) {
							lastinternalsleep = DateTime.Now;
							Thread.Sleep((random.Next() % 2) + 1);
						}
					}
					else if(thread.State == HSThreadState.SLEEP)
					{
						if(thread.AwakeTime < DateTime.Now)
						{
							thread.State = HSThreadState.RUNNING;
						}
					}
					else if(thread.State == HSThreadState.DEAD)
					{
						tdlist.Add(tid);
					}
				}
				
				// add pending threads to the thread dict
				if(PendingThreads.Count > 0)
				{
					foreach(int ti in PendingThreads.Keys)
					{
						threads[ti] = PendingThreads[ti];
					}
				}
				
				foreach(int i in tdlist)
				{
					threads.Remove(i);
				}
			}
		}
		
		public void Execute(HSThread thread, string line)
		{
			if(line[0] == ':')
			{
				return;
			}
			
			string[] arr = line.Split(new char[] { ' ' }, 2);
			string sOp = arr[0];
			HSOpCodeType op = (HSOpCodeType)Enum.Parse(typeof(HSOpCodeType), sOp);
			
			if(op == HSOpCodeType.add)
			{
				execute_add(thread);
			}
			else if(op == HSOpCodeType.and)
			{
				execute_and(thread);
			}
			else if(op == HSOpCodeType.call)
			{
				string sfunc = arr[1];
				execute_call(thread, sfunc);
			}
			else if(op == HSOpCodeType.div)
			{
				execute_div(thread);
			}
			else if(op == HSOpCodeType.eq)
			{
				execute_eq(thread);
			}
			else if(op == HSOpCodeType.jmp)
			{
				string slbl = arr[1];
				execute_jmp(thread, slbl);
			}
			else if(op == HSOpCodeType.jmpif)
			{
				string slbl = arr[1];
				execute_jmpif(thread, slbl);
			}
			else if(op == HSOpCodeType.lt)
			{
				execute_lt(thread);
			}
			else if(op == HSOpCodeType.lteq)
			{
				execute_lteq(thread);
			}
			else if(op == HSOpCodeType.mt)
			{
				execute_mt(thread);
			}
			else if(op == HSOpCodeType.mteq)
			{
				execute_mteq(thread);
			}
			else if(op == HSOpCodeType.mul)
			{
				execute_mul(thread);
			}
			else if(op == HSOpCodeType.neg)
			{
				execute_neg(thread);
			}
			else if(op == HSOpCodeType.neq)
			{
				execute_neq(thread);
			}
			else if(op == HSOpCodeType.or)
			{
				execute_or(thread);
			}
			else if(op == HSOpCodeType.pop)
			{
				string svar = string.Empty;
				if(arr.Length == 2)
				{
					svar = arr[1];
				}
				
				execute_pop(thread, svar);
			}
			else if(op == HSOpCodeType.push)
			{
				string arg = arr[1];
				execute_push(thread, arg);
			}
			else if(op == HSOpCodeType.ret)
			{
				execute_ret(thread);
			}
			else if(op == HSOpCodeType.sub)
			{
				execute_sub(thread);
			}
			else if(op == HSOpCodeType.sys)
			{
				string sfunc = arr[1];
				execute_sys(thread, sfunc);
			}
			else if(op == HSOpCodeType.var)
			{
				string svar = arr[1];
				execute_var(thread, svar);
			}
		}
		
		private void execute_sys(HSThread thread, string sfunc)
		{
			if(sysfuncs.ContainsKey(sfunc))
			{
				sysfuncs[sfunc](this, thread);
			}
		}
		
		#region Execution Helpers
		private void execute_var(HSThread thread, string svar)
		{
			if(thread == null)
			{
				scope.Storage.AddVariable(svar);
			}
			else
			{
				thread.CurrentFrame.CurrentScope.Storage.AddVariable(svar);
			}
		}
		
		private void execute_push(HSThread thread, string arg)
		{
			HSValueType type = getArgType(arg);
			
			HSValue pval = new HSValue();
			
			if(type == HSValueType.NUMBER)
			{
				pval.SetNumber(int.Parse(arg));
			}
			else if(type == HSValueType.STRING)
			{
				pval.SetString(getFixedString(arg.Substring(1,arg.Length-2)));
			}
			else
			{
				if(GlobalScope.Storage.GetFunction(arg) != null)
				{
					pval.SetVariable(arg);
				}
				else
				{
					HSVariable hvar = getVariable(thread, arg);
					if(hvar.Type == HSVariableType.NUMBER)
					{
						pval.SetNumber(hvar.NumberValue);
					}
					else
					{
						pval.SetString(hvar.StringValue);
					}
				}
			}
			
			thread.Context.Stack.Push(pval);
		}
		
		private void execute_pop(HSThread thread, string svar)
		{
			HSValue val = thread.Context.Stack.Pop();
			
			if(svar.Length > 0)
			{
				HSVariable v = getVariable(thread, svar);
				if(v != null)
				{
					if(val.Type == HSValueType.NUMBER)
					{
						v.Set(val.NumberValue);
					}
					else
					{
						v.Set(val.StringValue);
					}
				}
			}
		}
		
		private void execute_jmpif(HSThread thread, string label)
		{
			HSLabel lbl = thread.GlobalScope.Storage.GetLabel(label);
			
			HSValue val = thread.Context.Stack.Pop();
		 	
			if(lbl != null)
			{
				if(val.NumberValue == 0)
				{
					thread.Context.Pointer = lbl.Location;
				}
			}
		}
		
		private void execute_jmp(HSThread thread, string label)
		{
			HSLabel lbl = thread.GlobalScope.Storage.GetLabel(label);
		 	
			if(lbl != null)
			{
				thread.Context.Pointer = lbl.Location;
			}
		}
		
		private void execute_call(HSThread thread, string sfunc)
		{
			HSFunction func = thread.GlobalScope.Storage.GetFunction(sfunc);
			if(func != null)
			{
				// save the pointer
				thread.CurrentFrame.SavedPointer = thread.Context.Pointer;
				
				HSFrame frame = new HSFrame(thread.GlobalScope);
				
				thread.FrameStack.Push(frame);
				thread.CurrentFrame = frame;
				
				thread.Context.Pointer = func.Location;
			}
		}
		
		private void execute_ret(HSThread thread)
		{
			// destroy the previous frame stack
			HSFrame prevframe = thread.FrameStack.Pop();
			prevframe.Destroy();
			
			if(thread.FrameStack.Count > 0)
			{
				thread.CurrentFrame = thread.FrameStack.Peek();
				thread.Context.Pointer = thread.CurrentFrame.SavedPointer;
			}
		}
		
		private void execute_neg(HSThread thread)
		{
			HSValue v1 = thread.Context.Stack.Pop();
				
			int v1int = getIntValue(thread, v1);
			string v1str = getStringValue(thread, v1);
				
			HSValue ret = new HSValue();
				
			HSValueType vtype = v1.Type;
			
			if(vtype == HSValueType.NUMBER)
			{
				if(v1int == 0)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}
			else
			{
				if(v1str.Length == 0)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}

			thread.Context.Stack.Push(ret);
		}
		
		private void execute_mteq(HSThread thread)
		{
			HSValue v2 = thread.Context.Stack.Pop();
			HSValue v1 = thread.Context.Stack.Pop();
				
			int v1int = getIntValue(thread, v1);
			string v1str = getStringValue(thread, v1);
				
			int v2int = getIntValue(thread, v2);
			string v2str = getStringValue(thread, v2);
				
			HSValue ret = new HSValue();
				
			HSValueType vtype = v1.Type;
			
			if(vtype == HSValueType.NUMBER)
			{
				if(v1int >= v2int)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}
			else
			{
				if(v1str.Length >= v2str.Length)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}

			thread.Context.Stack.Push(ret);
		}
		
		private void execute_mt(HSThread thread)
		{
			HSValue v2 = thread.Context.Stack.Pop();
			HSValue v1 = thread.Context.Stack.Pop();
				
			int v1int = getIntValue(thread, v1);
			string v1str = getStringValue(thread, v1);
				
			int v2int = getIntValue(thread, v2);
			string v2str = getStringValue(thread, v2);
				
			HSValue ret = new HSValue();
				
			HSValueType vtype = v1.Type;
			
			if(vtype == HSValueType.NUMBER)
			{
				if(v1int > v2int)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}
			else
			{
				if(v1str.Length > v2str.Length)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}

			thread.Context.Stack.Push(ret);
		}
		
		private void execute_lteq(HSThread thread)
		{
			HSValue v2 = thread.Context.Stack.Pop();
			HSValue v1 = thread.Context.Stack.Pop();
				
			int v1int = getIntValue(thread, v1);
			string v1str = getStringValue(thread, v1);
				
			int v2int = getIntValue(thread, v2);
			string v2str = getStringValue(thread, v2);
				
			HSValue ret = new HSValue();
				
			HSValueType vtype = v1.Type;
			
			if(vtype == HSValueType.NUMBER)
			{
				if(v1int <= v2int)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}
			else
			{
				if(v1str.Length <= v2str.Length)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}

			thread.Context.Stack.Push(ret);
		}
		
		private void execute_lt(HSThread thread)
		{
			HSValue v2 = thread.Context.Stack.Pop();
			HSValue v1 = thread.Context.Stack.Pop();
				
			int v1int = getIntValue(thread, v1);
			string v1str = getStringValue(thread, v1);
				
			int v2int = getIntValue(thread, v2);
			string v2str = getStringValue(thread, v2);
				
			HSValue ret = new HSValue();
				
			HSValueType vtype = v1.Type;
			
			if(vtype == HSValueType.NUMBER)
			{
				if(v1int < v2int)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}
			else
			{
				if(v1str.Length < v2str.Length)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}

			thread.Context.Stack.Push(ret);
		}
		
		private void execute_eq(HSThread thread)
		{
			HSValue v2 = thread.Context.Stack.Pop();
			HSValue v1 = thread.Context.Stack.Pop();
				
			int v1int = getIntValue(thread, v1);
			string v1str = getStringValue(thread, v1);
				
			int v2int = getIntValue(thread, v2);
			string v2str = getStringValue(thread, v2);
				
			HSValue ret = new HSValue();
				
			HSValueType vtype = v1.Type;
			
			if(vtype == HSValueType.NUMBER)
			{
				if(v1int == v2int)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}
			else
			{
				if(v1str == v2str)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}

			thread.Context.Stack.Push(ret);
		}
		
		private void execute_neq(HSThread thread)
		{
			HSValue v2 = thread.Context.Stack.Pop();
			HSValue v1 = thread.Context.Stack.Pop();
				
			int v1int = getIntValue(thread, v1);
			string v1str = getStringValue(thread, v1);
				
			int v2int = getIntValue(thread, v2);
			string v2str = getStringValue(thread, v2);
				
			HSValue ret = new HSValue();
				
			HSValueType vtype = v1.Type;
			
			if(vtype == HSValueType.NUMBER)
			{
				if(v1int != v2int)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}
			else
			{
				if(v1str != v2str)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}

			thread.Context.Stack.Push(ret);
		}
		
		private void execute_mul(HSThread thread)
		{
			HSValue v2 = thread.Context.Stack.Pop();
			HSValue v1 = thread.Context.Stack.Pop();
				
			int v1int = getIntValue(thread, v1);
			string v1str = getStringValue(thread, v1);
				
			int v2int = getIntValue(thread, v2);
				
			HSValue ret = new HSValue();
				
			HSValueType vtype = v1.Type;
			
			if(vtype == HSValueType.NUMBER)
			{
				ret.SetNumber(v1int*v2int);
			}
			else
			{
				string nstr = "";
				for(int i=0; i<v2int; i++)
				{
					nstr += v1str;
				}
				ret.SetString(nstr);
			}
				
			thread.Context.Stack.Push(ret);
		}
		
		private void execute_div(HSThread thread)
		{
			HSValue v2 = thread.Context.Stack.Pop();
			HSValue v1 = thread.Context.Stack.Pop();
				
			int v1int = getIntValue(thread, v1);
			string v1str = getStringValue(thread, v1);
				
			int v2int = getIntValue(thread, v2);
				
			HSValue ret = new HSValue();
				
			HSValueType vtype = v1.Type;
			
			if(vtype == HSValueType.NUMBER)
			{
				ret.SetNumber(v1int/v2int);
			}
			else
			{
				ret.SetString(v1str);
			}
				
			thread.Context.Stack.Push(ret);
		}
		
		private void execute_and(HSThread thread)
		{
			HSValue v2 = thread.Context.Stack.Pop();
			HSValue v1 = thread.Context.Stack.Pop();
				
			int v1int = getIntValue(thread, v1);
			string v1str = getStringValue(thread, v1);
				
			int v2int = getIntValue(thread, v2);
			string v2str = getStringValue(thread, v2);
				
			HSValue ret = new HSValue();
				
			HSValueType vtype = v1.Type;
			
			if(vtype == HSValueType.NUMBER)
			{
				if(v1int>0 && v2int>0)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}
			else
			{
				if(v1str.Length > 0 && v2str.Length > 0)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}
				
			thread.Context.Stack.Push(ret);
		}
		
		private void execute_or(HSThread thread)
		{
			HSValue v2 = thread.Context.Stack.Pop();
			HSValue v1 = thread.Context.Stack.Pop();
				
			int v1int = getIntValue(thread, v1);
			string v1str = getStringValue(thread, v1);
				
			int v2int = getIntValue(thread, v2);
			string v2str = getStringValue(thread, v2);
				
			HSValue ret = new HSValue();
				
			HSValueType vtype = v1.Type;
			
			if(vtype == HSValueType.NUMBER)
			{
				if(v1int>0 || v2int>0)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}
			else
			{
				if(v1str.Length > 0 || v2str.Length > 0)
				{
					ret.SetNumber(1);
				}
				else
				{
					ret.SetNumber(0);
				}
			}
				
			thread.Context.Stack.Push(ret);
		}
		
		private void execute_add(HSThread thread)
		{
			HSValue v2 = thread.Context.Stack.Pop();
			HSValue v1 = thread.Context.Stack.Pop();
				
			int v1int = getIntValue(thread, v1);
				
			int v2int = getIntValue(thread, v2);
				
			HSValue ret = new HSValue();
			
			if(v1.Type == HSValueType.NUMBER && v2.Type == HSValueType.NUMBER)
			{
				ret.SetNumber(v1int + v2int);
			}
			else
			{
				string s1 = "";
				if(v1.Type == HSValueType.NUMBER)
				{
					s1 = v1.NumberValue.ToString();
				}
				else
				{
					s1 = v1.StringValue;
				}
				
				string s2 = "";
				if(v2.Type == HSValueType.NUMBER)
				{
					s2 = v2.NumberValue.ToString();
				}
				else
				{
					s2 = v2.StringValue;
				}
				
				ret.SetString(s1 + s2);
			}
				
			thread.Context.Stack.Push(ret);
		}
		
		private void execute_sub(HSThread thread)
		{
			HSValue v2 = thread.Context.Stack.Pop();
			HSValue v1 = thread.Context.Stack.Pop();
				
			int v1int = getIntValue(thread, v1);
			string v1str = getStringValue(thread, v1);
				
			int v2int = getIntValue(thread, v2);
				
			HSValue ret = new HSValue();
				
			HSValueType vtype = v1.Type;
			
			if(vtype == HSValueType.NUMBER)
			{
				ret.SetNumber(v1int - v2int);
			}
			else
			{
				ret.SetString(v1str);
			}
				
			thread.Context.Stack.Push(ret);
		}
		
		public HSVariable getVariable(HSThread thread, string name)
		{
			return thread.CurrentFrame.CurrentScope.GetVariable(name);
		}
		
		public string getStringValue(HSThread thread, HSValue val)
		{
			string ret = string.Empty;
			if(val.Type == HSValueType.VARIABLE)
			{
				HSVariable v = getVariable(thread, val.VariableValue);
				if(v == null)
				{
					ret = "";
				}
				else
				{
					ret = v.StringValue;
				}
			}
			else
			{
				ret = val.StringValue;
			}
			return ret;
		}
		
		public int getIntValue(HSThread thread, HSValue val)
		{
			int ret = 0;
			if(val.Type == HSValueType.VARIABLE)
			{
				HSVariable v = getVariable(thread, val.VariableValue);
				if(v == null)
				{
					ret = 0;
				}
				else
				{
					ret = v.NumberValue;
				}
			}
			else
			{
				ret = val.NumberValue;
			}
			return ret;
		}
		
		public HSValueType getArgType(string arg)
		{
			if(arg.Length == 0)
			{
				return HSValueType.VOID;
			}
			
			if(arg[0] == '"')
			{
				return HSValueType.STRING;
			}
			else if((arg[0] >= '0' && arg[0] <= '9') || (arg[0] == '+') || (arg[0] == '-'))
			{
				return HSValueType.NUMBER;
			}
			else
			{
				return HSValueType.VARIABLE;
			}
		}
		
		public HSValue getArgValue(string arg)
		{
			HSValueType vt = getArgType(arg);
			HSValue v = new HSValue();
			if(vt == HSValueType.STRING)
			{
				v.SetString(arg.Substring(1,arg.Length-2));
			}
			else if(vt == HSValueType.NUMBER)
			{
				v.SetNumber(int.Parse(arg));
			}
			else if(vt == HSValueType.VARIABLE)
			{
				v.SetVariable(arg);
			}
			
			return v;
		}
		
		public string getFixedString(string ufstr)
		{
			string ret = "";
			int sstate = 0;
			for(int i=0; i<ufstr.Length; i++)
			{
				char c = ufstr[i];
				if(sstate == 0)
				{
					if(c == '\\')
					{
						sstate = 1;
					}
					else
					{
						ret += c;
					}
				}
				else
				{
					if(c == 'b')
					{
						ret += '\b';
					}
					else if(c == 't')
					{
						ret += '\t';
					}
					else if(c == 'n')
					{
						ret += '\n';
					}
					else if(c == 'f')
					{
						ret += '\f';
					}
					else if(c == 'r')
					{
						ret += '\r';
					}
					else if(c == '"')
					{
						ret += '"';
					}
					else if(c == '\\')
					{
						ret += '\\';
					}
					else
					{
						ret += '\\';
						ret += c;
					}
					
					sstate = 0;
				}
			}
			return ret;
		}
		#endregion
		// Only to be used by the Machine itself to execute global
		// statements in bulk during scan!
		private void Execute(List<string> lines)
		{
			foreach(string line in lines)
			{
				Execute(null, line);
			}
		}
		
		public void Load(string code)
		{
			asm = code;
		}
		
		public int generateThreadId()
		{
			return ++threadidGen;
		}
	}
}
