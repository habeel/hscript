
using System;

namespace hscript.VirtualMachine
{
	public class HSLabel
	{
		public String Name { get; private set; }
		public Int32 Location { get; private set; }
		
		public HSLabel (string name, int loc)
		{
			Name = name;
			Location = loc;
		}
	}
}
