
using System;

namespace hscript.Compiler.Nodes
{
	public class HSReturn : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.RETURN;
		}
		
		#endregion
		
		public IHSNode Expression;
		
		public HSReturn ()
		{
			Expression = null;
		}
		
		public void Set(IHSNode expr)
		{
			Expression = expr;
		}
	}
}
