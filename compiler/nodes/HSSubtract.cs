
using System;

namespace hscript.Compiler.Nodes
{
	public class HSSubtract : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.SUBTRACT;
		}
		
		#endregion

		public IHSNode Expression1;
		public IHSNode Expression2;
		
		public HSSubtract (IHSNode op1, IHSNode op2)
		{
			Expression1 = op1;
			Expression2 = op2;
		}
	}
}
