
using System;
using System.Collections.Generic;

namespace hscript.Compiler.Nodes
{
	public class HSFunction : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.FUNCTION;
		}
		
		#endregion
		
		public String FuncName;
		public HSParameters Parameters;
		public List<IHSNode> Body;
		
		public HSFunction (string name)
		{
			FuncName = name;
			Parameters = null;
			Body = new List<IHSNode>();
		}
		
		public void SetParameters(HSParameters paras)
		{
			Parameters = paras;
		}
		
		public void Add(IHSNode stmt)
		{
			Body.Add(stmt);
		}
	}
}
