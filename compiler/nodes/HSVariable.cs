
using System;

namespace hscript.Compiler.Nodes
{
	public class HSVariable : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.VARIABLE;
		}
		
		#endregion
		
		public string VariableName;
		
		public HSVariable (string vname)
		{
			VariableName = vname;
		}
	}
}
