
using System;

namespace hscript.Compiler.Nodes
{
	public class HSNumber : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.NUMBER;
		}
		
		#endregion
		
		public int NumberLiteral;

		public HSNumber (int num)
		{
			NumberLiteral = num;
		}
	}
}
