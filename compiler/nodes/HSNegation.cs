
using System;

namespace hscript.Compiler.Nodes
{
	public class HSNegation : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.NEGATION;
		}
		
		#endregion
		
		public IHSNode Expression;
		
		public HSNegation (IHSNode expr)
		{
			Expression = expr;
		}
	}
}
