
using System;

namespace hscript.Compiler.Nodes
{
	public class HSFuncCall : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.FUNCCALL;
		}
		
		#endregion
		
		public string FuncName;
		public HSArguments Arguments;
		
		public HSFuncCall (string fname)
		{
			FuncName = fname;
			Arguments = null;
		}
		
		public void SetArguments(HSArguments args)
		{
			Arguments = args;
		}
	}
}
