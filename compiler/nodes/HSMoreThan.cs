
using System;

namespace hscript.Compiler.Nodes
{
	public class HSMoreThan : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.MORETHAN;
		}
		
		#endregion
		
		public IHSNode Expression1;
		public IHSNode Expression2;

		public HSMoreThan (IHSNode op1, IHSNode op2)
		{
			Expression1 = op1;
			Expression2 = op2;
		}
	}
}
