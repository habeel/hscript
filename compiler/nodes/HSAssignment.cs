
using System;

namespace hscript.Compiler.Nodes
{
	public class HSAssignment : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.ASSIGNMENT;
		}
		
		#endregion
		
		public HSVariable Variable;
		public IHSNode Expression;

		public HSAssignment (string vname, IHSNode expr)
		{
			Variable = new HSVariable(vname);
			Expression = expr;
		}
	}
}
