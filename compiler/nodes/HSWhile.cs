
using System;
using System.Collections.Generic;

namespace hscript.Compiler.Nodes
{
	public class HSWhile : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.WHILE;
		}
		
		#endregion
		
		public IHSNode Condition;
		public List<IHSNode> Body;
		
		public HSWhile (IHSNode expr)
		{
			Condition = expr;
			Body = new List<IHSNode>();
		}
		
		public void Add(IHSNode stmt)
		{
			Body.Add(stmt);
		}
	}
}
