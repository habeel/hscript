
using System;

namespace hscript.Compiler.Nodes
{
	public class HSAnd : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.AND;
		}
		
		#endregion
		
		public IHSNode Expression1;
		public IHSNode Expression2;
	
		public HSAnd (IHSNode op1, IHSNode op2)
		{
			Expression1 = op1;
			Expression2 = op2;
		}
	}
}
