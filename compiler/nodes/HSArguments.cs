
using System;
using System.Collections.Generic;

namespace hscript.Compiler.Nodes
{
	public class HSArguments : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.ARGUMENTS;
		}
		
		#endregion
		
		public List<IHSNode> ArgumentList;
		
		public HSArguments ()
		{
			ArgumentList = new List<IHSNode>();
		}
		
		public void Add(IHSNode arg)
		{
			ArgumentList.Add(arg);
		}
	}
}
