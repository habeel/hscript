
using System;
using System.Collections.Generic;

namespace hscript.Compiler.Nodes
{
	public class HSParameters : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.PARAMETERS;
		}
		
		#endregion
		
		public List<string> Parameters;
				
		public HSParameters ()
		{
			Parameters = new List<string>();
		}
		
		public void Add(string name)
		{
			Parameters.Add(name);
		}
	}
}
