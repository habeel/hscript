
using System;

namespace hscript.Compiler.Nodes
{
	public enum HSNodeType
	{
		NUMBER,
		STRING,
		VARIABLE,
		NEGATION,
		NEGATIVE,
		MULTIPLY,
		DIVIDE,
		ADDITION,
		SUBTRACT,
		EQUALTO,
		NOTEQUALTO,
		MORETHAN,
		LESSTHAN,
		MORETHANEQUALTO,
		LESSTHANEQUALTO,
		AND,
		OR,
		ARGUMENTS,
		FUNCCALL,
		SYSCALL,
		RETURN,
		IF,
		WHILE,
		VARDECLARE,
		ASSIGNMENT,
		PARAMETERS,
		FUNCTION,
		PROGRAM
	}
	
	public interface IHSNode
	{
		HSNodeType GetNodeType();
	}
}
