
using System;

namespace hscript.Compiler.Nodes
{
	public class HSLessThan : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.LESSTHAN;
		}
		
		#endregion
		
		public IHSNode Expression1;
		public IHSNode Expression2;

		public HSLessThan (IHSNode op1, IHSNode op2)
		{
			Expression1 = op1;
			Expression2 = op2;
		}
	}
}
