
using System;

namespace hscript.Compiler.Nodes
{
	public class HSNegative : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.NEGATIVE;
		}
		
		#endregion
		
		public IHSNode Expression;

		public HSNegative (IHSNode expr)
		{
			Expression = expr;
		}
	}
}
