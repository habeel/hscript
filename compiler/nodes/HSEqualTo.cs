
using System;

namespace hscript.Compiler.Nodes
{


	public class HSEqualTo : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.EQUALTO;
		}
		
		#endregion
		
		public IHSNode Expression1;
		public IHSNode Expression2;

		public HSEqualTo (IHSNode op1, IHSNode op2)
		{
			Expression1 = op1;
			Expression2 = op2;
		}
	}
}
