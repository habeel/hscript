
using System;
using System.Collections.Generic;

namespace hscript.Compiler.Nodes
{
	public class HSProgram : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.PROGRAM;
		}
		
		#endregion
		
		public List<IHSNode> GlobalStatements;

		public HSProgram ()
		{
			GlobalStatements = new List<IHSNode>();
		}
		
		public void Add(IHSNode globstmt)
		{
			GlobalStatements.Add(globstmt);
		}
	}
}
