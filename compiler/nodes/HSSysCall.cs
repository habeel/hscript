
using System;

namespace hscript.Compiler.Nodes
{
	public class HSSysCall : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.SYSCALL;
		}
		
		#endregion
		
		public string FuncName;
		public HSArguments Arguments;
		
		public HSSysCall (string fname)
		{
			FuncName = fname;
			Arguments = null;
		}
		
		public void SetArguments(HSArguments args)
		{
			Arguments = args;
		}
	}
}
