
using System;

namespace hscript.Compiler.Nodes
{
	public class HSOr : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.OR;
		}
		
		#endregion
		
		public IHSNode Expression1;
		public IHSNode Expression2;

		public HSOr (IHSNode op1, IHSNode op2)
		{
			Expression1 = op1;
			Expression2 = op2;
		}
	}
}
