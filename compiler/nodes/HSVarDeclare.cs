
using System;

namespace hscript.Compiler.Nodes
{


	public class HSVarDeclare : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.VARDECLARE;
		}
		
		#endregion
		
		public string VariableName;
		public IHSNode Expression;

		public HSVarDeclare (string vname)
		{
			VariableName = vname;
			Expression = null;
		}
		
		public HSVarDeclare(string vname, IHSNode expr)
		{
			VariableName = vname;
			Expression = expr;
		}
	}
}
