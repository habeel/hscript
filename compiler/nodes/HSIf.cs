
using System;
using System.Collections.Generic;

namespace hscript.Compiler.Nodes
{
	public class HSIf : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.IF;
		}
		
		#endregion

		public IHSNode Condition;
		public List<IHSNode> Body;
		
		public HSIf (IHSNode expr)
		{
			Condition = expr;
			Body = new List<IHSNode>();
		}
		
		public void Add(IHSNode stmt)
		{
			Body.Add(stmt);
		}
	}
}
