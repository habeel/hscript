
using System;

namespace hscript.Compiler.Nodes
{
	public class HSString : IHSNode
	{
		#region IHSNode implementation
		public HSNodeType GetNodeType ()
		{
			return HSNodeType.STRING;
		}
		
		#endregion
		
		public string StringLiteral;
		
		public HSString (string str)
		{
			StringLiteral = str;
		}
	}
}
