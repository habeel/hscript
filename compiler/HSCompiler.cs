
using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using Antlr.Runtime;
using Antlr.Runtime.Tree;

using hscript.Compiler.Nodes;

namespace hscript.Compiler
{
	public class HSCompiler
	{
		public struct HSValue
		{
			public int Number;
			public string String;
		}
		
		private int count;
		private bool used;
		private string code;
		private string hasm;
		private List<string> loadedfiles = new List<string>();
		private string libdir;
		
		public HSCompiler ()
		{
			count = 0;
			used = false;
			code = string.Empty;
			hasm = string.Empty;
			loadedfiles = new List<string>();
			
			libdir = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "/lib/hscript/";
		}
		
		public void Load(string filename)
		{
			if(used) { return; }
			
			// if the file is already loaded dont load it twice!
			if(loadedfiles.Contains(filename))
			{
				return;
			}
			
			loadedfiles.Add(filename);
			
			StringBuilder sb = new StringBuilder();
			StreamReader reader = new StreamReader(new FileStream(filename, FileMode.Open, FileAccess.Read));
			
			while(!reader.EndOfStream)
			{
				string line = reader.ReadLine();
				if(line.StartsWith("load" ))
				{
					string fname = line.Substring(line.IndexOf(" ")+1);
					
					if(fname.Length > 0)
					{
						// first check if the included file exists as part of a library
						if(File.Exists(libdir + fname + ".hs"))
						{
							Load(libdir + fname + ".hs");
						}
						else
						{
							if(fname[0] == '*') { fname = fname.Substring(1); }
							if(Path.GetDirectoryName(filename) == "")
							{
								Load(fname + ".hs");
							}
							else
							{
								Load(Path.GetDirectoryName(filename) + "/" + fname + ".hs");
							}
						}
					}
				}
				else
				{
					sb.AppendLine(line);
					sb.Append(reader.ReadToEnd());
					break;
				}
			}
			
			code += sb.ToString();
		}
		
		public string Compile()
		{
			if(used) { return hasm; }
			ANTLRStringStream stream = new ANTLRStringStream(code);
			
			HScriptLexer lexer = new HScriptLexer(stream);
			
			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			
			HScriptParser parser = new HScriptParser(tokenStream);
			
			IHSNode program = parser.program().n;
			
			hasm = Generate(program);
			used = true;
			
			return hasm;
		}
		
		private string Generate(IHSNode node)
		{
			string output = "";
			if(node.GetNodeType() == HSNodeType.PROGRAM)
			{
				HSProgram program = (HSProgram)node;
				
				foreach(IHSNode cnode in program.GlobalStatements)
				{
					output += Generate(cnode);
				}
			}
			else if(node.GetNodeType() == HSNodeType.VARDECLARE)
			{
				HSVarDeclare vdec = (HSVarDeclare)node;
				
				output += generateDeclareVariable(vdec.VariableName);
				
				if(vdec.Expression != null)
				{
					output += generateAssignVariable(new HSAssignment(vdec.VariableName, vdec.Expression));
				}
			}
			else if(node.GetNodeType() == HSNodeType.FUNCTION)
			{
				HSFunction func = (HSFunction)node;
				
				output += generateFunction(func);
			}
			else if(node.GetNodeType() == HSNodeType.ASSIGNMENT)
			{
				HSAssignment asgn = (HSAssignment)node;
				
				output += generateAssignVariable(asgn);
			}
			else if(node.GetNodeType() == HSNodeType.RETURN)
			{
				HSReturn ret = (HSReturn)node;
				
				output += generateReturn(ret);
			}
			else if(node.GetNodeType() == HSNodeType.FUNCCALL)
			{
				HSFuncCall fcall = (HSFuncCall)node;
				
				output += generateFuncCall(fcall);
			}
			else if(node.GetNodeType() == HSNodeType.SYSCALL)
			{
				HSSysCall scall = (HSSysCall)node;
				
				output += generateSysCall(scall);
			}
			else if(node.GetNodeType() == HSNodeType.IF)
			{
				HSIf hif = (HSIf)node;
				/*
					push condition
					jmpif endlbl
					statements*
					:endlbl
				*/
				output += generateIf(hif);
			}
			else if(node.GetNodeType() == HSNodeType.WHILE)
			{
				HSWhile hwhile = (HSWhile)node;
				/*
				 	:beglbl
				 	push condition
				 	jmpif endlbl
				 	statements*
				 	jmp beglbl
				 	:endlbl
				*/
				output += generateWhile(hwhile);
			}
			// Expressions
			else if(node.GetNodeType() == HSNodeType.NUMBER)
			{
				HSNumber hnum = (HSNumber)node;
				output += generatePush(hnum);
			}
			else if(node.GetNodeType() == HSNodeType.VARIABLE)
			{
				HSVariable hvar = (HSVariable)node;
				output += generatePush(hvar);
			}
			else if(node.GetNodeType() == HSNodeType.STRING)
			{
				HSString hstr = (HSString)node;
				output += generatePush(hstr);
			}
			else if(node.GetNodeType() == HSNodeType.NEGATION)
			{
				HSNegation neg = (HSNegation)node;
				output += generateNegation(neg);
			}
			else if(node.GetNodeType() == HSNodeType.NEGATIVE)
			{
				HSNegative nega = (HSNegative)node;
				output += generateNegative(nega);
			}
			else if(node.GetNodeType() == HSNodeType.MULTIPLY)
			{
				HSMultiply mult = (HSMultiply)node;
				output += generateMultiply(mult);
			}
			else if(node.GetNodeType() == HSNodeType.DIVIDE)
			{
				HSDivide div = (HSDivide)node;
				output += generateDivide(div);
			}
			else if(node.GetNodeType() == HSNodeType.ADDITION)
			{
				HSAddition hadd = (HSAddition)node;
				output += generateAddition(hadd);
			}
			else if(node.GetNodeType() == HSNodeType.SUBTRACT)
			{
				HSSubtract sub = (HSSubtract)node;
				output += generateSubtract(sub);
			}
			else if(node.GetNodeType() == HSNodeType.EQUALTO)
			{
				HSEqualTo eq = (HSEqualTo)node;
				output += generateEqualTo(eq);
			}
			else if(node.GetNodeType() == HSNodeType.NOTEQUALTO)
			{
				HSNotEqualTo neq = (HSNotEqualTo)node;
				output += generateNotEqualTo(neq);
			}
			else if(node.GetNodeType() == HSNodeType.MORETHAN)
			{
				HSMoreThan mt = (HSMoreThan)node;
				output += generateMoreThan(mt);
			}
			else if(node.GetNodeType() == HSNodeType.MORETHANEQUALTO)
			{
				HSMoreThanEqualTo mteq = (HSMoreThanEqualTo)node;
				output += generateMoreThanEqualTo(mteq);
			}
			else if(node.GetNodeType() == HSNodeType.LESSTHAN)
			{
				HSLessThan lt = (HSLessThan)node;
				output += generateLessThan(lt);
			}
			else if(node.GetNodeType() == HSNodeType.LESSTHANEQUALTO)
			{
				HSLessThanEqualTo lteq = (HSLessThanEqualTo)node;
				output += generateLessThanEqualTo(lteq);
			}
			else if(node.GetNodeType() == HSNodeType.OR)
			{
				HSOr or = (HSOr)node;
				output += generateOr(or);
			}
			else if(node.GetNodeType() == HSNodeType.AND)
			{
				HSAnd and = (HSAnd)node;
				output += generateAnd(and);
			}
			
			return output;
		}
		
		private string getUniqueLabel()
		{
			count++;
			return "lbl" + count.ToString();
		}
		
		private string generateFunction(HSFunction func)
		{
			string output = "";
			
			// Func start (LABEL)
			output += String.Format("::{0}\n", func.FuncName);
			
			// Parameters
			if(func.Parameters != null)
			{
				output += generateParameters(func.Parameters);
			}
			
			// Body
			foreach(IHSNode stmt in func.Body)
			{
				output += Generate(stmt);
			}
			
			output += generateReturn(new HSReturn());
			
			return output;
		}
		
		private string generateParameters(HSParameters param)
		{
			string output = "";
			foreach(string p in param.Parameters)
			{
				output += generateDeclareVariable(p);
				output += generatePop(new HSVariable(p));
			}
			return output;
		}
		
		private string generateOr(HSOr or)
		{
			return generateDualExp("or", or.Expression1, or.Expression2);
		}
		
		private string generateAnd(HSAnd and)
		{
			return generateDualExp("and", and.Expression1, and.Expression2);
		}
		
		private string generateEqualTo(HSEqualTo eq)
		{
			return generateDualExp("eq", eq.Expression1, eq.Expression2);
		}
		
		private string generateNotEqualTo(HSNotEqualTo neq)
		{
			return generateDualExp("neq", neq.Expression1, neq.Expression2);
		}
		
		private string generateMoreThan(HSMoreThan mt)
		{
			return generateDualExp("mt", mt.Expression1, mt.Expression2);
		}
		
		private string generateMoreThanEqualTo(HSMoreThanEqualTo mteq)
		{
			return generateDualExp("mteq", mteq.Expression1, mteq.Expression2);
		}
		
		private string generateLessThan(HSLessThan lt)
		{
			return generateDualExp("lt", lt.Expression1, lt.Expression2);
		}
		
		private string generateLessThanEqualTo(HSLessThanEqualTo lteq)
		{
			return generateDualExp("eq", lteq.Expression1, lteq.Expression2);
		}
		
		private string generateAddition(HSAddition hadd)
		{
			return generateDualExp("add", hadd.Expression1, hadd.Expression2);
		}
		
		private string generateSubtract(HSSubtract sub)
		{
			return generateDualExp("sub", sub.Expression1, sub.Expression2);
		}
		
		private string generateMultiply(HSMultiply mult)
		{
			return generateDualExp("mul", mult.Expression1, mult.Expression2);
		}
		
		private string generateDivide(HSDivide div)
		{
			return generateDualExp("div", div.Expression1, div.Expression2);
		}
		
		private string generateDualExp(string term, IHSNode exp1, IHSNode exp2)
		{
			string output = "";
			output += Generate(exp1);
			output += Generate(exp2);
			
			output += String.Format("{0}\n", term);
			
			return output;
		}
		
		private string generateNegative(HSNegative hnega)
		{
			return Generate(new HSMultiply(hnega.Expression, new HSNumber(-1)));
		}
		
		private string generateNegation(HSNegation hneg)
		{
			string output = Generate(hneg.Expression);
			output += "neg\n";
			return output;
		}
		
		private string generatePush(HSNumber hnum)
		{
			return String.Format("push {0}\n", hnum.NumberLiteral);
		}
		
		private string generatePush(HSVariable hvar)
		{
			return String.Format("push {0}\n", hvar.VariableName);
		}
		
		private string generatePush(HSString hstr)
		{
			return String.Format("push {0}\n", hstr.StringLiteral);
		}
		
		private string generateIf(HSIf hif)
		{
			string endlbl = getUniqueLabel();
			
			string output = "";
			
			// compile the condition (will cause the condition to be pushed)
			output += Generate(hif.Condition);
			
			output += String.Format("jmpif {0}\n", endlbl);
			
			foreach(IHSNode node in hif.Body)
			{
				output += Generate(node);
			}
			
			output += String.Format(":{0}\n", endlbl);
			
			return output;
		}
		
		private string generateWhile(HSWhile hwhile)
		{
			string output = "";
			string beglbl = getUniqueLabel();
			string endlbl = getUniqueLabel();
			
			output += String.Format(":{0}\n", beglbl);
			
			output += Generate(hwhile.Condition);
			
			output += String.Format("jmpif {0}\n", endlbl);
			
			foreach(IHSNode node in hwhile.Body)
			{
				output += Generate(node);
			}
			
			output += String.Format("jmp {0}\n", beglbl);
			output += String.Format(":{0}\n", endlbl);
			
			return output;
		}
		
		private string generateSysCall(HSSysCall scall)
		{
			string output = "";
			if(scall.Arguments != null)
			{
				output += generateArguments(scall.Arguments);
			}
			
			output += String.Format("sys {0}\n", scall.FuncName);
			
			return output;
		}
		
		private string generateFuncCall(HSFuncCall fcall)
		{
			string output = "";
			if(fcall.Arguments != null)
			{
				output += generateArguments(fcall.Arguments);
			}
			
			output += String.Format("call {0}\n", fcall.FuncName);
			
			return output;
		}
		
		private string generateArguments(HSArguments args)
		{
			List<IHSNode> rargs = new List<IHSNode>(args.ArgumentList);
			rargs.Reverse();
			
			string output = "";
			foreach(IHSNode exp in rargs)
			{
				output += Generate(exp);
			}
			
			return output;
		}
		
		private string generateReturn(HSReturn ret)
		{
			string output = "";
			if(ret.Expression != null)
			{
				output = Generate(ret.Expression);
			}
			output += "ret\n";
			return output;
		}
		
		private string generateDeclareVariable(string name)
		{
			return String.Format("var {0}\n", name);
		}
		
		private string generateAssignVariable(HSAssignment asn)
		{
			string output = Generate(asn.Expression);
			
			output += generatePop(asn.Variable);
			
			return output;
		}
		
		public string generatePop()
		{
			return "pop\n";
		}
		
		private string generatePop(HSVariable v)
		{
			return String.Format("pop {0}\n", v.VariableName);
		}
		
		#region Test
		public void Test(string filename) {
			ANTLRFileStream stream = new ANTLRFileStream(filename);
			
			HScriptLexer lexer = new HScriptLexer(stream);
			
			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			
			HScriptParser parser = new HScriptParser(tokenStream);
			
			IHSNode expr = parser.expression().n;
			
			GenerateExpression(expr);
		}
		
		public void GenerateExpression(IHSNode expr)
		{
			if(expr.GetNodeType() == HSNodeType.NUMBER)
			{
				HSNumber num = (HSNumber)expr;
				Console.WriteLine(String.Format("push {0}", num.NumberLiteral));
			}
			else if(expr.GetNodeType() == HSNodeType.STRING)
			{
				HSString str = (HSString)expr;
				Console.WriteLine(String.Format("push \"{0}\"", str.StringLiteral));
			}
			else if(expr.GetNodeType() == HSNodeType.VARIABLE)
			{
				HSVariable v = (HSVariable)expr;
				Console.WriteLine(String.Format("push {0}", v.VariableName));
			}
			else if(expr.GetNodeType() == HSNodeType.NEGATION)
			{
				HSNegation neg = (HSNegation)expr;
				GenerateExpression(neg.Expression);
				Console.WriteLine("neg");
			}
			else if(expr.GetNodeType() == HSNodeType.NEGATIVE)
			{
				HSNegative nega = (HSNegative)expr;
				GenerateExpression(nega.Expression);
				Console.WriteLine("push -1");
				Console.WriteLine("mul");
			}
			else if(expr.GetNodeType() == HSNodeType.MULTIPLY)
			{
				HSMultiply mul = (HSMultiply)expr;
				GenerateExpression(mul.Expression1);
				GenerateExpression(mul.Expression2);
				Console.WriteLine("mul");
			}
			else if(expr.GetNodeType() == HSNodeType.DIVIDE)
			{
				HSDivide div = (HSDivide)expr;
				GenerateExpression(div.Expression1);
				GenerateExpression(div.Expression2);
				Console.WriteLine("div");
			}
			else if(expr.GetNodeType() == HSNodeType.ADDITION)
			{
				HSAddition addi = (HSAddition)expr;
				GenerateExpression(addi.Expression1);
				GenerateExpression(addi.Expression2);
				Console.WriteLine("add");
			}
			else if(expr.GetNodeType() == HSNodeType.SUBTRACT)
			{
				HSSubtract sub = (HSSubtract)expr;
				GenerateExpression(sub.Expression1);
				GenerateExpression(sub.Expression2);
				Console.WriteLine("sub");
			}
			else if(expr.GetNodeType() == HSNodeType.EQUALTO)
			{
				HSEqualTo eq = (HSEqualTo)expr;
				GenerateExpression(eq.Expression1);
				GenerateExpression(eq.Expression2);
				Console.WriteLine("eq");
			}
			else if(expr.GetNodeType() == HSNodeType.NOTEQUALTO)
			{
				HSNotEqualTo neq = (HSNotEqualTo)expr;
				GenerateExpression(neq.Expression1);
				GenerateExpression(neq.Expression2);
				Console.WriteLine("neq");
			}
			else if(expr.GetNodeType() == HSNodeType.LESSTHAN)
			{
				HSLessThan lt = (HSLessThan)expr;
				GenerateExpression(lt.Expression1);
				GenerateExpression(lt.Expression2);
				Console.WriteLine("lt");
			}
			else if(expr.GetNodeType() == HSNodeType.LESSTHANEQUALTO)
			{
				HSLessThanEqualTo lteq = (HSLessThanEqualTo)expr;
				GenerateExpression(lteq.Expression1);
				GenerateExpression(lteq.Expression2);
				Console.WriteLine("lteq");
			}
			else if(expr.GetNodeType() == HSNodeType.MORETHAN)
			{
				HSMoreThan mt = (HSMoreThan)expr;
				GenerateExpression(mt.Expression1);
				GenerateExpression(mt.Expression2);
				Console.WriteLine("mt");
			}
			else if(expr.GetNodeType() == HSNodeType.MORETHANEQUALTO)
			{
				HSMoreThanEqualTo mteq = (HSMoreThanEqualTo)expr;
				GenerateExpression(mteq.Expression1);
				GenerateExpression(mteq.Expression2);
				Console.WriteLine("mteq");
			}
			else if(expr.GetNodeType() == HSNodeType.OR)
			{
				HSOr hor = (HSOr)expr;
				GenerateExpression(hor.Expression1);
				GenerateExpression(hor.Expression2);
				Console.WriteLine("or");
			}
			else if(expr.GetNodeType() == HSNodeType.AND)
			{
				HSAnd hand = (HSAnd)expr;
				GenerateExpression(hand.Expression1);
				GenerateExpression(hand.Expression2);
				Console.WriteLine("and");
			}
			else
			{
				Console.WriteLine("invalid expression type");
			}
		}
		#endregion
	}
}
