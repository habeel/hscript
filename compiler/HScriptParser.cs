// $ANTLR 3.2 Sep 23, 2009 12:02:23 /home/habeel/Programming/Projects/nettest/hscript/HScript.g 2010-08-29 19:29:10

// The variable 'variable' is assigned but its value is never used.
#pragma warning disable 168, 219
// Unreachable code detected.
#pragma warning disable 162


	using hscript.Compiler.Nodes;


using System;
using Antlr.Runtime;
using IList 		= System.Collections.IList;
using ArrayList 	= System.Collections.ArrayList;
using Stack 		= Antlr.Runtime.Collections.StackList;



using Antlr.Runtime.Tree;

namespace hscript.Compiler
{
public partial class HScriptParser : Parser
{
    public static readonly string[] tokenNames = new string[] 
	{
        "<invalid>", 
		"<EOR>", 
		"<DOWN>", 
		"<UP>", 
		"NEWLINE", 
		"IDENT", 
		"STRING", 
		"NUMBER", 
		"STRING_CHAR", 
		"ESC_SEQ", 
		"COMMENT", 
		"WS", 
		"','", 
		"'var'", 
		"'func'", 
		"'('", 
		"')'", 
		"'{'", 
		"'}'", 
		"'='", 
		"'if'", 
		"'while'", 
		"'return'", 
		"'$ys.'", 
		"'!'", 
		"'+'", 
		"'-'", 
		"'*'", 
		"'/'", 
		"'=='", 
		"'!='", 
		"'<'", 
		"'<='", 
		"'>'", 
		"'>='", 
		"'||'", 
		"'&&'"
    };

    public const int T__29 = 29;
    public const int T__28 = 28;
    public const int T__27 = 27;
    public const int T__26 = 26;
    public const int T__25 = 25;
    public const int T__24 = 24;
    public const int T__23 = 23;
    public const int T__22 = 22;
    public const int T__21 = 21;
    public const int T__20 = 20;
    public const int NUMBER = 7;
    public const int EOF = -1;
    public const int T__30 = 30;
    public const int T__19 = 19;
    public const int T__31 = 31;
    public const int T__32 = 32;
    public const int WS = 11;
    public const int ESC_SEQ = 9;
    public const int STRING_CHAR = 8;
    public const int T__33 = 33;
    public const int T__16 = 16;
    public const int T__34 = 34;
    public const int T__15 = 15;
    public const int NEWLINE = 4;
    public const int T__35 = 35;
    public const int T__18 = 18;
    public const int T__36 = 36;
    public const int T__17 = 17;
    public const int T__12 = 12;
    public const int T__14 = 14;
    public const int T__13 = 13;
    public const int IDENT = 5;
    public const int COMMENT = 10;
    public const int STRING = 6;

    // delegates
    // delegators



        public HScriptParser(ITokenStream input)
    		: this(input, new RecognizerSharedState()) {
        }

        public HScriptParser(ITokenStream input, RecognizerSharedState state)
    		: base(input, state) {
            InitializeCyclicDFAs();

             
        }
        
    protected ITreeAdaptor adaptor = new CommonTreeAdaptor();

    public ITreeAdaptor TreeAdaptor
    {
        get { return this.adaptor; }
        set {
    	this.adaptor = value;
    	}
    }

    override public string[] TokenNames {
		get { return HScriptParser.tokenNames; }
    }

    override public string GrammarFileName {
		get { return "/home/habeel/Programming/Projects/nettest/hscript/HScript.g"; }
    }


    public class program_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "program"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:17:1: program returns [IHSNode n] : ( NEWLINE )? ( global_statement )* ( NEWLINE )? EOF ;
    public HScriptParser.program_return program() // throws RecognitionException [1]
    {   
        HScriptParser.program_return retval = new HScriptParser.program_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken NEWLINE1 = null;
        IToken NEWLINE3 = null;
        IToken EOF4 = null;
        HScriptParser.global_statement_return global_statement2 = default(HScriptParser.global_statement_return);


        CommonTree NEWLINE1_tree=null;
        CommonTree NEWLINE3_tree=null;
        CommonTree EOF4_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:18:2: ( ( NEWLINE )? ( global_statement )* ( NEWLINE )? EOF )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:18:4: ( NEWLINE )? ( global_statement )* ( NEWLINE )? EOF
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	 HSProgram tmpp = new HSProgram(); 
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:19:2: ( NEWLINE )?
            	int alt1 = 2;
            	int LA1_0 = input.LA(1);

            	if ( (LA1_0 == NEWLINE) )
            	{
            	    alt1 = 1;
            	}
            	switch (alt1) 
            	{
            	    case 1 :
            	        // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:19:2: NEWLINE
            	        {
            	        	NEWLINE1=(IToken)Match(input,NEWLINE,FOLLOW_NEWLINE_in_program64); 
            	        		NEWLINE1_tree = (CommonTree)adaptor.Create(NEWLINE1);
            	        		adaptor.AddChild(root_0, NEWLINE1_tree);


            	        }
            	        break;

            	}

            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:20:2: ( global_statement )*
            	do 
            	{
            	    int alt2 = 2;
            	    int LA2_0 = input.LA(1);

            	    if ( (LA2_0 == NEWLINE || (LA2_0 >= 13 && LA2_0 <= 14)) )
            	    {
            	        alt2 = 1;
            	    }


            	    switch (alt2) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:21:3: global_statement
            			    {
            			    	PushFollow(FOLLOW_global_statement_in_program73);
            			    	global_statement2 = global_statement();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, global_statement2.Tree);

            			    				if(((global_statement2 != null) ? global_statement2.n : default(IHSNode)) != null) {
            			    					tmpp.Add(((global_statement2 != null) ? global_statement2.n : default(IHSNode)));
            			    				}
            			    			

            			    }
            			    break;

            			default:
            			    goto loop2;
            	    }
            	} while (true);

            	loop2:
            		;	// Stops C# compiler whining that label 'loop2' has no statements

            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:28:2: ( NEWLINE )?
            	int alt3 = 2;
            	int LA3_0 = input.LA(1);

            	if ( (LA3_0 == NEWLINE) )
            	{
            	    alt3 = 1;
            	}
            	switch (alt3) 
            	{
            	    case 1 :
            	        // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:28:2: NEWLINE
            	        {
            	        	NEWLINE3=(IToken)Match(input,NEWLINE,FOLLOW_NEWLINE_in_program85); 
            	        		NEWLINE3_tree = (CommonTree)adaptor.Create(NEWLINE3);
            	        		adaptor.AddChild(root_0, NEWLINE3_tree);


            	        }
            	        break;

            	}

            	EOF4=(IToken)Match(input,EOF,FOLLOW_EOF_in_program88); 
            		EOF4_tree = (CommonTree)adaptor.Create(EOF4);
            		adaptor.AddChild(root_0, EOF4_tree);


            			retval.n =  (IHSNode)tmpp;
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "program"

    public class global_statement_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "global_statement"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:34:1: global_statement returns [IHSNode n] : ( global_var_stmt | func_stmt | NEWLINE );
    public HScriptParser.global_statement_return global_statement() // throws RecognitionException [1]
    {   
        HScriptParser.global_statement_return retval = new HScriptParser.global_statement_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken NEWLINE7 = null;
        HScriptParser.global_var_stmt_return global_var_stmt5 = default(HScriptParser.global_var_stmt_return);

        HScriptParser.func_stmt_return func_stmt6 = default(HScriptParser.func_stmt_return);


        CommonTree NEWLINE7_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:35:2: ( global_var_stmt | func_stmt | NEWLINE )
            int alt4 = 3;
            switch ( input.LA(1) ) 
            {
            case 13:
            	{
                alt4 = 1;
                }
                break;
            case 14:
            	{
                alt4 = 2;
                }
                break;
            case NEWLINE:
            	{
                alt4 = 3;
                }
                break;
            	default:
            	    NoViableAltException nvae_d4s0 =
            	        new NoViableAltException("", 4, 0, input);

            	    throw nvae_d4s0;
            }

            switch (alt4) 
            {
                case 1 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:35:4: global_var_stmt
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_global_var_stmt_in_global_statement106);
                    	global_var_stmt5 = global_var_stmt();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, global_var_stmt5.Tree);
                    	 retval.n =  ((global_var_stmt5 != null) ? global_var_stmt5.n : default(IHSNode)); 

                    }
                    break;
                case 2 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:36:4: func_stmt
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_func_stmt_in_global_statement115);
                    	func_stmt6 = func_stmt();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, func_stmt6.Tree);
                    	 retval.n =  ((func_stmt6 != null) ? func_stmt6.n : default(IHSNode)); 

                    }
                    break;
                case 3 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:37:4: NEWLINE
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	NEWLINE7=(IToken)Match(input,NEWLINE,FOLLOW_NEWLINE_in_global_statement124); 
                    		NEWLINE7_tree = (CommonTree)adaptor.Create(NEWLINE7);
                    		adaptor.AddChild(root_0, NEWLINE7_tree);

                    	 retval.n =  null; 

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "global_statement"

    public class parameters_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "parameters"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:41:1: parameters returns [IHSNode n] : p1= IDENT ( ',' p2= IDENT )* ;
    public HScriptParser.parameters_return parameters() // throws RecognitionException [1]
    {   
        HScriptParser.parameters_return retval = new HScriptParser.parameters_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken p1 = null;
        IToken p2 = null;
        IToken char_literal8 = null;

        CommonTree p1_tree=null;
        CommonTree p2_tree=null;
        CommonTree char_literal8_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:42:2: (p1= IDENT ( ',' p2= IDENT )* )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:42:4: p1= IDENT ( ',' p2= IDENT )*
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	p1=(IToken)Match(input,IDENT,FOLLOW_IDENT_in_parameters147); 
            		p1_tree = (CommonTree)adaptor.Create(p1);
            		adaptor.AddChild(root_0, p1_tree);

            	 HSParameters paras = new HSParameters(); paras.Add(p1.Text); 
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:43:3: ( ',' p2= IDENT )*
            	do 
            	{
            	    int alt5 = 2;
            	    int LA5_0 = input.LA(1);

            	    if ( (LA5_0 == 12) )
            	    {
            	        alt5 = 1;
            	    }


            	    switch (alt5) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:43:4: ',' p2= IDENT
            			    {
            			    	char_literal8=(IToken)Match(input,12,FOLLOW_12_in_parameters154); 
            			    		char_literal8_tree = (CommonTree)adaptor.Create(char_literal8);
            			    		adaptor.AddChild(root_0, char_literal8_tree);

            			    	p2=(IToken)Match(input,IDENT,FOLLOW_IDENT_in_parameters158); 
            			    		p2_tree = (CommonTree)adaptor.Create(p2);
            			    		adaptor.AddChild(root_0, p2_tree);

            			    	 paras.Add(p2.Text); 

            			    }
            			    break;

            			default:
            			    goto loop5;
            	    }
            	} while (true);

            	loop5:
            		;	// Stops C# compiler whining that label 'loop5' has no statements

            	 retval.n =  (IHSNode)paras; 

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "parameters"

    public class global_var_stmt_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "global_var_stmt"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:48:1: global_var_stmt returns [IHSNode n] : 'var' IDENT NEWLINE ;
    public HScriptParser.global_var_stmt_return global_var_stmt() // throws RecognitionException [1]
    {   
        HScriptParser.global_var_stmt_return retval = new HScriptParser.global_var_stmt_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken string_literal9 = null;
        IToken IDENT10 = null;
        IToken NEWLINE11 = null;

        CommonTree string_literal9_tree=null;
        CommonTree IDENT10_tree=null;
        CommonTree NEWLINE11_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:49:2: ( 'var' IDENT NEWLINE )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:49:4: 'var' IDENT NEWLINE
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	string_literal9=(IToken)Match(input,13,FOLLOW_13_in_global_var_stmt185); 
            		string_literal9_tree = (CommonTree)adaptor.Create(string_literal9);
            		adaptor.AddChild(root_0, string_literal9_tree);

            	IDENT10=(IToken)Match(input,IDENT,FOLLOW_IDENT_in_global_var_stmt187); 
            		IDENT10_tree = (CommonTree)adaptor.Create(IDENT10);
            		adaptor.AddChild(root_0, IDENT10_tree);

            	NEWLINE11=(IToken)Match(input,NEWLINE,FOLLOW_NEWLINE_in_global_var_stmt189); 
            		NEWLINE11_tree = (CommonTree)adaptor.Create(NEWLINE11);
            		adaptor.AddChild(root_0, NEWLINE11_tree);

            	 retval.n =  new HSVarDeclare(IDENT10.Text); 

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "global_var_stmt"

    public class func_stmt_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "func_stmt"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:52:1: func_stmt returns [IHSNode n] : 'func' IDENT '(' ( parameters )? ')' '{' ( statement )* '}' NEWLINE ;
    public HScriptParser.func_stmt_return func_stmt() // throws RecognitionException [1]
    {   
        HScriptParser.func_stmt_return retval = new HScriptParser.func_stmt_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken string_literal12 = null;
        IToken IDENT13 = null;
        IToken char_literal14 = null;
        IToken char_literal16 = null;
        IToken char_literal17 = null;
        IToken char_literal19 = null;
        IToken NEWLINE20 = null;
        HScriptParser.parameters_return parameters15 = default(HScriptParser.parameters_return);

        HScriptParser.statement_return statement18 = default(HScriptParser.statement_return);


        CommonTree string_literal12_tree=null;
        CommonTree IDENT13_tree=null;
        CommonTree char_literal14_tree=null;
        CommonTree char_literal16_tree=null;
        CommonTree char_literal17_tree=null;
        CommonTree char_literal19_tree=null;
        CommonTree NEWLINE20_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:53:2: ( 'func' IDENT '(' ( parameters )? ')' '{' ( statement )* '}' NEWLINE )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:53:4: 'func' IDENT '(' ( parameters )? ')' '{' ( statement )* '}' NEWLINE
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	string_literal12=(IToken)Match(input,14,FOLLOW_14_in_func_stmt210); 
            		string_literal12_tree = (CommonTree)adaptor.Create(string_literal12);
            		adaptor.AddChild(root_0, string_literal12_tree);

            	IDENT13=(IToken)Match(input,IDENT,FOLLOW_IDENT_in_func_stmt212); 
            		IDENT13_tree = (CommonTree)adaptor.Create(IDENT13);
            		adaptor.AddChild(root_0, IDENT13_tree);

            	char_literal14=(IToken)Match(input,15,FOLLOW_15_in_func_stmt217); 
            		char_literal14_tree = (CommonTree)adaptor.Create(char_literal14);
            		adaptor.AddChild(root_0, char_literal14_tree);


            					HSFunction hfunc = new HSFunction(IDENT13.Text);
            				
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:58:5: ( parameters )?
            	int alt6 = 2;
            	int LA6_0 = input.LA(1);

            	if ( (LA6_0 == IDENT) )
            	{
            	    alt6 = 1;
            	}
            	switch (alt6) 
            	{
            	    case 1 :
            	        // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:59:6: parameters
            	        {
            	        	PushFollow(FOLLOW_parameters_in_func_stmt235);
            	        	parameters15 = parameters();
            	        	state.followingStackPointer--;

            	        	adaptor.AddChild(root_0, parameters15.Tree);

            	        			 			HSParameters hparam = (HSParameters)((parameters15 != null) ? parameters15.n : default(IHSNode));
            	        			 			hfunc.SetParameters(hparam);
            	        			 		

            	        }
            	        break;

            	}

            	char_literal16=(IToken)Match(input,16,FOLLOW_16_in_func_stmt254); 
            		char_literal16_tree = (CommonTree)adaptor.Create(char_literal16);
            		adaptor.AddChild(root_0, char_literal16_tree);

            	char_literal17=(IToken)Match(input,17,FOLLOW_17_in_func_stmt259); 
            		char_literal17_tree = (CommonTree)adaptor.Create(char_literal17);
            		adaptor.AddChild(root_0, char_literal17_tree);

            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:67:4: ( statement )*
            	do 
            	{
            	    int alt7 = 2;
            	    int LA7_0 = input.LA(1);

            	    if ( ((LA7_0 >= NEWLINE && LA7_0 <= IDENT) || LA7_0 == 13 || (LA7_0 >= 20 && LA7_0 <= 23)) )
            	    {
            	        alt7 = 1;
            	    }


            	    switch (alt7) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:68:5: statement
            			    {
            			    	PushFollow(FOLLOW_statement_in_func_stmt270);
            			    	statement18 = statement();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, statement18.Tree);

            			    						if(((statement18 != null) ? statement18.n : default(IHSNode)) != null) 
            			    						{
            			    							hfunc.Add(((statement18 != null) ? statement18.n : default(IHSNode)));
            			    						}
            			    					

            			    }
            			    break;

            			default:
            			    goto loop7;
            	    }
            	} while (true);

            	loop7:
            		;	// Stops C# compiler whining that label 'loop7' has no statements

            	char_literal19=(IToken)Match(input,18,FOLLOW_18_in_func_stmt287); 
            		char_literal19_tree = (CommonTree)adaptor.Create(char_literal19);
            		adaptor.AddChild(root_0, char_literal19_tree);

            	NEWLINE20=(IToken)Match(input,NEWLINE,FOLLOW_NEWLINE_in_func_stmt289); 
            		NEWLINE20_tree = (CommonTree)adaptor.Create(NEWLINE20);
            		adaptor.AddChild(root_0, NEWLINE20_tree);


            				retval.n =  (IHSNode)hfunc;
            			

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "func_stmt"

    public class statement_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "statement"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:83:1: statement returns [IHSNode n] : ( var_stmt | assign_stmt | if_stmt | while_stmt | return_stmt | syscall_stmt | funccall_stmt | NEWLINE );
    public HScriptParser.statement_return statement() // throws RecognitionException [1]
    {   
        HScriptParser.statement_return retval = new HScriptParser.statement_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken NEWLINE28 = null;
        HScriptParser.var_stmt_return var_stmt21 = default(HScriptParser.var_stmt_return);

        HScriptParser.assign_stmt_return assign_stmt22 = default(HScriptParser.assign_stmt_return);

        HScriptParser.if_stmt_return if_stmt23 = default(HScriptParser.if_stmt_return);

        HScriptParser.while_stmt_return while_stmt24 = default(HScriptParser.while_stmt_return);

        HScriptParser.return_stmt_return return_stmt25 = default(HScriptParser.return_stmt_return);

        HScriptParser.syscall_stmt_return syscall_stmt26 = default(HScriptParser.syscall_stmt_return);

        HScriptParser.funccall_stmt_return funccall_stmt27 = default(HScriptParser.funccall_stmt_return);


        CommonTree NEWLINE28_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:84:2: ( var_stmt | assign_stmt | if_stmt | while_stmt | return_stmt | syscall_stmt | funccall_stmt | NEWLINE )
            int alt8 = 8;
            alt8 = dfa8.Predict(input);
            switch (alt8) 
            {
                case 1 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:84:4: var_stmt
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_var_stmt_in_statement309);
                    	var_stmt21 = var_stmt();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, var_stmt21.Tree);
                    	 retval.n =  ((var_stmt21 != null) ? var_stmt21.n : default(IHSNode)); 

                    }
                    break;
                case 2 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:85:4: assign_stmt
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_assign_stmt_in_statement318);
                    	assign_stmt22 = assign_stmt();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, assign_stmt22.Tree);
                    	 retval.n =  ((assign_stmt22 != null) ? assign_stmt22.n : default(IHSNode)); 

                    }
                    break;
                case 3 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:86:4: if_stmt
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_if_stmt_in_statement327);
                    	if_stmt23 = if_stmt();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, if_stmt23.Tree);
                    	 retval.n =  ((if_stmt23 != null) ? if_stmt23.n : default(IHSNode)); 

                    }
                    break;
                case 4 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:87:4: while_stmt
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_while_stmt_in_statement337);
                    	while_stmt24 = while_stmt();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, while_stmt24.Tree);
                    	 retval.n =  ((while_stmt24 != null) ? while_stmt24.n : default(IHSNode)); 

                    }
                    break;
                case 5 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:88:4: return_stmt
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_return_stmt_in_statement346);
                    	return_stmt25 = return_stmt();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, return_stmt25.Tree);
                    	 retval.n =  ((return_stmt25 != null) ? return_stmt25.n : default(IHSNode)); 

                    }
                    break;
                case 6 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:89:4: syscall_stmt
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_syscall_stmt_in_statement355);
                    	syscall_stmt26 = syscall_stmt();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, syscall_stmt26.Tree);
                    	 retval.n =  ((syscall_stmt26 != null) ? syscall_stmt26.n : default(IHSNode)); 

                    }
                    break;
                case 7 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:90:4: funccall_stmt
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_funccall_stmt_in_statement363);
                    	funccall_stmt27 = funccall_stmt();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, funccall_stmt27.Tree);
                    	 retval.n =  ((funccall_stmt27 != null) ? funccall_stmt27.n : default(IHSNode)); 

                    }
                    break;
                case 8 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:91:4: NEWLINE
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	NEWLINE28=(IToken)Match(input,NEWLINE,FOLLOW_NEWLINE_in_statement371); 
                    		NEWLINE28_tree = (CommonTree)adaptor.Create(NEWLINE28);
                    		adaptor.AddChild(root_0, NEWLINE28_tree);

                    	 retval.n =  null; 

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "statement"

    public class var_stmt_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "var_stmt"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:94:1: var_stmt returns [IHSNode n] : ( 'var' IDENT NEWLINE | 'var' IDENT '=' expression NEWLINE );
    public HScriptParser.var_stmt_return var_stmt() // throws RecognitionException [1]
    {   
        HScriptParser.var_stmt_return retval = new HScriptParser.var_stmt_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken string_literal29 = null;
        IToken IDENT30 = null;
        IToken NEWLINE31 = null;
        IToken string_literal32 = null;
        IToken IDENT33 = null;
        IToken char_literal34 = null;
        IToken NEWLINE36 = null;
        HScriptParser.expression_return expression35 = default(HScriptParser.expression_return);


        CommonTree string_literal29_tree=null;
        CommonTree IDENT30_tree=null;
        CommonTree NEWLINE31_tree=null;
        CommonTree string_literal32_tree=null;
        CommonTree IDENT33_tree=null;
        CommonTree char_literal34_tree=null;
        CommonTree NEWLINE36_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:95:2: ( 'var' IDENT NEWLINE | 'var' IDENT '=' expression NEWLINE )
            int alt9 = 2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0 == 13) )
            {
                int LA9_1 = input.LA(2);

                if ( (LA9_1 == IDENT) )
                {
                    int LA9_2 = input.LA(3);

                    if ( (LA9_2 == NEWLINE) )
                    {
                        alt9 = 1;
                    }
                    else if ( (LA9_2 == 19) )
                    {
                        alt9 = 2;
                    }
                    else 
                    {
                        NoViableAltException nvae_d9s2 =
                            new NoViableAltException("", 9, 2, input);

                        throw nvae_d9s2;
                    }
                }
                else 
                {
                    NoViableAltException nvae_d9s1 =
                        new NoViableAltException("", 9, 1, input);

                    throw nvae_d9s1;
                }
            }
            else 
            {
                NoViableAltException nvae_d9s0 =
                    new NoViableAltException("", 9, 0, input);

                throw nvae_d9s0;
            }
            switch (alt9) 
            {
                case 1 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:95:4: 'var' IDENT NEWLINE
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	string_literal29=(IToken)Match(input,13,FOLLOW_13_in_var_stmt391); 
                    		string_literal29_tree = (CommonTree)adaptor.Create(string_literal29);
                    		adaptor.AddChild(root_0, string_literal29_tree);

                    	IDENT30=(IToken)Match(input,IDENT,FOLLOW_IDENT_in_var_stmt393); 
                    		IDENT30_tree = (CommonTree)adaptor.Create(IDENT30);
                    		adaptor.AddChild(root_0, IDENT30_tree);

                    	NEWLINE31=(IToken)Match(input,NEWLINE,FOLLOW_NEWLINE_in_var_stmt395); 
                    		NEWLINE31_tree = (CommonTree)adaptor.Create(NEWLINE31);
                    		adaptor.AddChild(root_0, NEWLINE31_tree);

                    	 retval.n =  new HSVarDeclare(IDENT30.Text); 

                    }
                    break;
                case 2 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:96:4: 'var' IDENT '=' expression NEWLINE
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	string_literal32=(IToken)Match(input,13,FOLLOW_13_in_var_stmt406); 
                    		string_literal32_tree = (CommonTree)adaptor.Create(string_literal32);
                    		adaptor.AddChild(root_0, string_literal32_tree);

                    	IDENT33=(IToken)Match(input,IDENT,FOLLOW_IDENT_in_var_stmt408); 
                    		IDENT33_tree = (CommonTree)adaptor.Create(IDENT33);
                    		adaptor.AddChild(root_0, IDENT33_tree);

                    	char_literal34=(IToken)Match(input,19,FOLLOW_19_in_var_stmt410); 
                    		char_literal34_tree = (CommonTree)adaptor.Create(char_literal34);
                    		adaptor.AddChild(root_0, char_literal34_tree);

                    	PushFollow(FOLLOW_expression_in_var_stmt412);
                    	expression35 = expression();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, expression35.Tree);
                    	NEWLINE36=(IToken)Match(input,NEWLINE,FOLLOW_NEWLINE_in_var_stmt414); 
                    		NEWLINE36_tree = (CommonTree)adaptor.Create(NEWLINE36);
                    		adaptor.AddChild(root_0, NEWLINE36_tree);

                    	 retval.n =  new HSVarDeclare(IDENT33.Text, ((expression35 != null) ? expression35.n : default(IHSNode))); 

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "var_stmt"

    public class assign_stmt_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "assign_stmt"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:99:1: assign_stmt returns [IHSNode n] : IDENT '=' expression NEWLINE ;
    public HScriptParser.assign_stmt_return assign_stmt() // throws RecognitionException [1]
    {   
        HScriptParser.assign_stmt_return retval = new HScriptParser.assign_stmt_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken IDENT37 = null;
        IToken char_literal38 = null;
        IToken NEWLINE40 = null;
        HScriptParser.expression_return expression39 = default(HScriptParser.expression_return);


        CommonTree IDENT37_tree=null;
        CommonTree char_literal38_tree=null;
        CommonTree NEWLINE40_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:100:2: ( IDENT '=' expression NEWLINE )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:100:4: IDENT '=' expression NEWLINE
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	IDENT37=(IToken)Match(input,IDENT,FOLLOW_IDENT_in_assign_stmt432); 
            		IDENT37_tree = (CommonTree)adaptor.Create(IDENT37);
            		adaptor.AddChild(root_0, IDENT37_tree);

            	char_literal38=(IToken)Match(input,19,FOLLOW_19_in_assign_stmt434); 
            		char_literal38_tree = (CommonTree)adaptor.Create(char_literal38);
            		adaptor.AddChild(root_0, char_literal38_tree);

            	PushFollow(FOLLOW_expression_in_assign_stmt436);
            	expression39 = expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, expression39.Tree);
            	NEWLINE40=(IToken)Match(input,NEWLINE,FOLLOW_NEWLINE_in_assign_stmt438); 
            		NEWLINE40_tree = (CommonTree)adaptor.Create(NEWLINE40);
            		adaptor.AddChild(root_0, NEWLINE40_tree);

            	 retval.n =  new HSAssignment(IDENT37.Text, ((expression39 != null) ? expression39.n : default(IHSNode))); 

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "assign_stmt"

    public class if_stmt_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "if_stmt"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:103:1: if_stmt returns [IHSNode n] : 'if' expression '{' ( statement )* '}' NEWLINE ;
    public HScriptParser.if_stmt_return if_stmt() // throws RecognitionException [1]
    {   
        HScriptParser.if_stmt_return retval = new HScriptParser.if_stmt_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken string_literal41 = null;
        IToken char_literal43 = null;
        IToken char_literal45 = null;
        IToken NEWLINE46 = null;
        HScriptParser.expression_return expression42 = default(HScriptParser.expression_return);

        HScriptParser.statement_return statement44 = default(HScriptParser.statement_return);


        CommonTree string_literal41_tree=null;
        CommonTree char_literal43_tree=null;
        CommonTree char_literal45_tree=null;
        CommonTree NEWLINE46_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:104:2: ( 'if' expression '{' ( statement )* '}' NEWLINE )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:104:4: 'if' expression '{' ( statement )* '}' NEWLINE
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	string_literal41=(IToken)Match(input,20,FOLLOW_20_in_if_stmt457); 
            		string_literal41_tree = (CommonTree)adaptor.Create(string_literal41);
            		adaptor.AddChild(root_0, string_literal41_tree);

            	PushFollow(FOLLOW_expression_in_if_stmt459);
            	expression42 = expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, expression42.Tree);
            	char_literal43=(IToken)Match(input,17,FOLLOW_17_in_if_stmt461); 
            		char_literal43_tree = (CommonTree)adaptor.Create(char_literal43);
            		adaptor.AddChild(root_0, char_literal43_tree);

            	 HSIf hif = new HSIf(((expression42 != null) ? expression42.n : default(IHSNode))); 
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:105:5: ( statement )*
            	do 
            	{
            	    int alt10 = 2;
            	    int LA10_0 = input.LA(1);

            	    if ( ((LA10_0 >= NEWLINE && LA10_0 <= IDENT) || LA10_0 == 13 || (LA10_0 >= 20 && LA10_0 <= 23)) )
            	    {
            	        alt10 = 1;
            	    }


            	    switch (alt10) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:106:6: statement
            			    {
            			    	PushFollow(FOLLOW_statement_in_if_stmt477);
            			    	statement44 = statement();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, statement44.Tree);

            			    			 			if(((statement44 != null) ? statement44.n : default(IHSNode)) != null)
            			    			 			{
            			    			 				hif.Add(((statement44 != null) ? statement44.n : default(IHSNode)));
            			    			 			}
            			    			 		

            			    }
            			    break;

            			default:
            			    goto loop10;
            	    }
            	} while (true);

            	loop10:
            		;	// Stops C# compiler whining that label 'loop10' has no statements

            	char_literal45=(IToken)Match(input,18,FOLLOW_18_in_if_stmt496); 
            		char_literal45_tree = (CommonTree)adaptor.Create(char_literal45);
            		adaptor.AddChild(root_0, char_literal45_tree);

            	NEWLINE46=(IToken)Match(input,NEWLINE,FOLLOW_NEWLINE_in_if_stmt498); 
            		NEWLINE46_tree = (CommonTree)adaptor.Create(NEWLINE46);
            		adaptor.AddChild(root_0, NEWLINE46_tree);


            				retval.n =  (IHSNode)hif;
            			

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "if_stmt"

    public class while_stmt_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "while_stmt"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:120:1: while_stmt returns [IHSNode n] : 'while' expression '{' ( statement )* '}' NEWLINE ;
    public HScriptParser.while_stmt_return while_stmt() // throws RecognitionException [1]
    {   
        HScriptParser.while_stmt_return retval = new HScriptParser.while_stmt_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken string_literal47 = null;
        IToken char_literal49 = null;
        IToken char_literal51 = null;
        IToken NEWLINE52 = null;
        HScriptParser.expression_return expression48 = default(HScriptParser.expression_return);

        HScriptParser.statement_return statement50 = default(HScriptParser.statement_return);


        CommonTree string_literal47_tree=null;
        CommonTree char_literal49_tree=null;
        CommonTree char_literal51_tree=null;
        CommonTree NEWLINE52_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:121:2: ( 'while' expression '{' ( statement )* '}' NEWLINE )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:121:4: 'while' expression '{' ( statement )* '}' NEWLINE
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	string_literal47=(IToken)Match(input,21,FOLLOW_21_in_while_stmt518); 
            		string_literal47_tree = (CommonTree)adaptor.Create(string_literal47);
            		adaptor.AddChild(root_0, string_literal47_tree);

            	PushFollow(FOLLOW_expression_in_while_stmt520);
            	expression48 = expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, expression48.Tree);
            	char_literal49=(IToken)Match(input,17,FOLLOW_17_in_while_stmt522); 
            		char_literal49_tree = (CommonTree)adaptor.Create(char_literal49);
            		adaptor.AddChild(root_0, char_literal49_tree);

            	 HSWhile hwhile = new HSWhile(((expression48 != null) ? expression48.n : default(IHSNode))); 
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:122:5: ( statement )*
            	do 
            	{
            	    int alt11 = 2;
            	    int LA11_0 = input.LA(1);

            	    if ( ((LA11_0 >= NEWLINE && LA11_0 <= IDENT) || LA11_0 == 13 || (LA11_0 >= 20 && LA11_0 <= 23)) )
            	    {
            	        alt11 = 1;
            	    }


            	    switch (alt11) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:123:6: statement
            			    {
            			    	PushFollow(FOLLOW_statement_in_while_stmt537);
            			    	statement50 = statement();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, statement50.Tree);

            			    		 				if(((statement50 != null) ? statement50.n : default(IHSNode)) != null) {
            			    		 					hwhile.Add(((statement50 != null) ? statement50.n : default(IHSNode)));
            			    		 				}
            			    		 			

            			    }
            			    break;

            			default:
            			    goto loop11;
            	    }
            	} while (true);

            	loop11:
            		;	// Stops C# compiler whining that label 'loop11' has no statements

            	char_literal51=(IToken)Match(input,18,FOLLOW_18_in_while_stmt557); 
            		char_literal51_tree = (CommonTree)adaptor.Create(char_literal51);
            		adaptor.AddChild(root_0, char_literal51_tree);

            	NEWLINE52=(IToken)Match(input,NEWLINE,FOLLOW_NEWLINE_in_while_stmt559); 
            		NEWLINE52_tree = (CommonTree)adaptor.Create(NEWLINE52);
            		adaptor.AddChild(root_0, NEWLINE52_tree);


            				retval.n =  (IHSNode)hwhile;
            			

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "while_stmt"

    public class return_stmt_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "return_stmt"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:136:1: return_stmt returns [IHSNode n] : 'return' ( expression )? NEWLINE ;
    public HScriptParser.return_stmt_return return_stmt() // throws RecognitionException [1]
    {   
        HScriptParser.return_stmt_return retval = new HScriptParser.return_stmt_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken string_literal53 = null;
        IToken NEWLINE55 = null;
        HScriptParser.expression_return expression54 = default(HScriptParser.expression_return);


        CommonTree string_literal53_tree=null;
        CommonTree NEWLINE55_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:137:2: ( 'return' ( expression )? NEWLINE )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:137:4: 'return' ( expression )? NEWLINE
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	string_literal53=(IToken)Match(input,22,FOLLOW_22_in_return_stmt580); 
            		string_literal53_tree = (CommonTree)adaptor.Create(string_literal53);
            		adaptor.AddChild(root_0, string_literal53_tree);


            				HSReturn hret = new HSReturn();
            			
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:141:3: ( expression )?
            	int alt12 = 2;
            	int LA12_0 = input.LA(1);

            	if ( ((LA12_0 >= IDENT && LA12_0 <= NUMBER) || LA12_0 == 15 || (LA12_0 >= 23 && LA12_0 <= 26)) )
            	{
            	    alt12 = 1;
            	}
            	switch (alt12) 
            	{
            	    case 1 :
            	        // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:142:4: expression
            	        {
            	        	PushFollow(FOLLOW_expression_in_return_stmt593);
            	        	expression54 = expression();
            	        	state.followingStackPointer--;

            	        	adaptor.AddChild(root_0, expression54.Tree);

            	        					hret.Set(((expression54 != null) ? expression54.n : default(IHSNode)));
            	        				

            	        }
            	        break;

            	}

            	NEWLINE55=(IToken)Match(input,NEWLINE,FOLLOW_NEWLINE_in_return_stmt605); 
            		NEWLINE55_tree = (CommonTree)adaptor.Create(NEWLINE55);
            		adaptor.AddChild(root_0, NEWLINE55_tree);

            	 retval.n =  (IHSNode)hret; 

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "return_stmt"

    public class syscall_stmt_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "syscall_stmt"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:150:1: syscall_stmt returns [IHSNode n] : syscall NEWLINE ;
    public HScriptParser.syscall_stmt_return syscall_stmt() // throws RecognitionException [1]
    {   
        HScriptParser.syscall_stmt_return retval = new HScriptParser.syscall_stmt_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken NEWLINE57 = null;
        HScriptParser.syscall_return syscall56 = default(HScriptParser.syscall_return);


        CommonTree NEWLINE57_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:151:2: ( syscall NEWLINE )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:151:4: syscall NEWLINE
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_syscall_in_syscall_stmt626);
            	syscall56 = syscall();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, syscall56.Tree);
            	NEWLINE57=(IToken)Match(input,NEWLINE,FOLLOW_NEWLINE_in_syscall_stmt628); 
            		NEWLINE57_tree = (CommonTree)adaptor.Create(NEWLINE57);
            		adaptor.AddChild(root_0, NEWLINE57_tree);


            				retval.n =  ((syscall56 != null) ? syscall56.n : default(IHSNode));
            			

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "syscall_stmt"

    public class syscall_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "syscall"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:157:1: syscall returns [IHSNode n] : '$ys.' IDENT '(' ( args )? ')' ;
    public HScriptParser.syscall_return syscall() // throws RecognitionException [1]
    {   
        HScriptParser.syscall_return retval = new HScriptParser.syscall_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken string_literal58 = null;
        IToken IDENT59 = null;
        IToken char_literal60 = null;
        IToken char_literal62 = null;
        HScriptParser.args_return args61 = default(HScriptParser.args_return);


        CommonTree string_literal58_tree=null;
        CommonTree IDENT59_tree=null;
        CommonTree char_literal60_tree=null;
        CommonTree char_literal62_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:158:2: ( '$ys.' IDENT '(' ( args )? ')' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:158:4: '$ys.' IDENT '(' ( args )? ')'
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	string_literal58=(IToken)Match(input,23,FOLLOW_23_in_syscall648); 
            		string_literal58_tree = (CommonTree)adaptor.Create(string_literal58);
            		adaptor.AddChild(root_0, string_literal58_tree);

            	IDENT59=(IToken)Match(input,IDENT,FOLLOW_IDENT_in_syscall650); 
            		IDENT59_tree = (CommonTree)adaptor.Create(IDENT59);
            		adaptor.AddChild(root_0, IDENT59_tree);

            	char_literal60=(IToken)Match(input,15,FOLLOW_15_in_syscall654); 
            		char_literal60_tree = (CommonTree)adaptor.Create(char_literal60);
            		adaptor.AddChild(root_0, char_literal60_tree);


            					HSSysCall scall = new HSSysCall(IDENT59.Text);
            				
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:163:5: ( args )?
            	int alt13 = 2;
            	int LA13_0 = input.LA(1);

            	if ( ((LA13_0 >= IDENT && LA13_0 <= NUMBER) || LA13_0 == 15 || (LA13_0 >= 23 && LA13_0 <= 26)) )
            	{
            	    alt13 = 1;
            	}
            	switch (alt13) 
            	{
            	    case 1 :
            	        // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:164:6: args
            	        {
            	        	PushFollow(FOLLOW_args_in_syscall672);
            	        	args61 = args();
            	        	state.followingStackPointer--;

            	        	adaptor.AddChild(root_0, args61.Tree);

            	        			 			HSArguments sargs = (HSArguments)((args61 != null) ? args61.n : default(IHSNode));
            	        			 			scall.SetArguments(sargs);
            	        			 		

            	        }
            	        break;

            	}

            	char_literal62=(IToken)Match(input,16,FOLLOW_16_in_syscall691); 
            		char_literal62_tree = (CommonTree)adaptor.Create(char_literal62);
            		adaptor.AddChild(root_0, char_literal62_tree);


            			 	retval.n =  (IHSNode)scall;
            			 

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "syscall"

    public class funccall_stmt_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "funccall_stmt"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:176:1: funccall_stmt returns [IHSNode n] : funccall NEWLINE ;
    public HScriptParser.funccall_stmt_return funccall_stmt() // throws RecognitionException [1]
    {   
        HScriptParser.funccall_stmt_return retval = new HScriptParser.funccall_stmt_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken NEWLINE64 = null;
        HScriptParser.funccall_return funccall63 = default(HScriptParser.funccall_return);


        CommonTree NEWLINE64_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:177:2: ( funccall NEWLINE )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:177:4: funccall NEWLINE
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_funccall_in_funccall_stmt712);
            	funccall63 = funccall();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, funccall63.Tree);
            	NEWLINE64=(IToken)Match(input,NEWLINE,FOLLOW_NEWLINE_in_funccall_stmt714); 
            		NEWLINE64_tree = (CommonTree)adaptor.Create(NEWLINE64);
            		adaptor.AddChild(root_0, NEWLINE64_tree);


            				retval.n =  ((funccall63 != null) ? funccall63.n : default(IHSNode));
            			

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "funccall_stmt"

    public class funccall_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "funccall"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:183:1: funccall returns [IHSNode n] : IDENT '(' ( args )? ')' ;
    public HScriptParser.funccall_return funccall() // throws RecognitionException [1]
    {   
        HScriptParser.funccall_return retval = new HScriptParser.funccall_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken IDENT65 = null;
        IToken char_literal66 = null;
        IToken char_literal68 = null;
        HScriptParser.args_return args67 = default(HScriptParser.args_return);


        CommonTree IDENT65_tree=null;
        CommonTree char_literal66_tree=null;
        CommonTree char_literal68_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:184:2: ( IDENT '(' ( args )? ')' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:184:4: IDENT '(' ( args )? ')'
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	IDENT65=(IToken)Match(input,IDENT,FOLLOW_IDENT_in_funccall734); 
            		IDENT65_tree = (CommonTree)adaptor.Create(IDENT65);
            		adaptor.AddChild(root_0, IDENT65_tree);

            	char_literal66=(IToken)Match(input,15,FOLLOW_15_in_funccall739); 
            		char_literal66_tree = (CommonTree)adaptor.Create(char_literal66);
            		adaptor.AddChild(root_0, char_literal66_tree);


            					HSFuncCall fcall = new HSFuncCall(IDENT65.Text); 
            				
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:189:4: ( args )?
            	int alt14 = 2;
            	int LA14_0 = input.LA(1);

            	if ( ((LA14_0 >= IDENT && LA14_0 <= NUMBER) || LA14_0 == 15 || (LA14_0 >= 23 && LA14_0 <= 26)) )
            	{
            	    alt14 = 1;
            	}
            	switch (alt14) 
            	{
            	    case 1 :
            	        // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:190:5: args
            	        {
            	        	PushFollow(FOLLOW_args_in_funccall755);
            	        	args67 = args();
            	        	state.followingStackPointer--;

            	        	adaptor.AddChild(root_0, args67.Tree);

            	        						HSArguments fargs = (HSArguments)((args67 != null) ? args67.n : default(IHSNode));
            	        						fcall.SetArguments(fargs);
            	        					

            	        }
            	        break;

            	}

            	char_literal68=(IToken)Match(input,16,FOLLOW_16_in_funccall772); 
            		char_literal68_tree = (CommonTree)adaptor.Create(char_literal68);
            		adaptor.AddChild(root_0, char_literal68_tree);


            				retval.n =  (IHSNode)fcall;
            			

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "funccall"

    public class args_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "args"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:202:1: args returns [IHSNode n] : fr= expression ( ',' rr= expression )* ;
    public HScriptParser.args_return args() // throws RecognitionException [1]
    {   
        HScriptParser.args_return retval = new HScriptParser.args_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken char_literal69 = null;
        HScriptParser.expression_return fr = default(HScriptParser.expression_return);

        HScriptParser.expression_return rr = default(HScriptParser.expression_return);


        CommonTree char_literal69_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:203:2: (fr= expression ( ',' rr= expression )* )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:203:4: fr= expression ( ',' rr= expression )*
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_expression_in_args794);
            	fr = expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, fr.Tree);
            	 
            				HSArguments arg = new HSArguments();
            				arg.Add(((fr != null) ? fr.n : default(IHSNode)));
            			
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:208:3: ( ',' rr= expression )*
            	do 
            	{
            	    int alt15 = 2;
            	    int LA15_0 = input.LA(1);

            	    if ( (LA15_0 == 12) )
            	    {
            	        alt15 = 1;
            	    }


            	    switch (alt15) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:208:4: ',' rr= expression
            			    {
            			    	char_literal69=(IToken)Match(input,12,FOLLOW_12_in_args804); 
            			    		char_literal69_tree = (CommonTree)adaptor.Create(char_literal69);
            			    		adaptor.AddChild(root_0, char_literal69_tree);

            			    	PushFollow(FOLLOW_expression_in_args811);
            			    	rr = expression();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, rr.Tree);

            			    				arg.Add(((rr != null) ? rr.n : default(IHSNode)));
            			    			

            			    }
            			    break;

            			default:
            			    goto loop15;
            	    }
            	} while (true);

            	loop15:
            		;	// Stops C# compiler whining that label 'loop15' has no statements


            				retval.n =  arg;
            			

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "args"

    public class term_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "term"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:220:1: term returns [IHSNode n] : ( IDENT | '(' expression ')' | STRING | NUMBER | syscall | funccall );
    public HScriptParser.term_return term() // throws RecognitionException [1]
    {   
        HScriptParser.term_return retval = new HScriptParser.term_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken IDENT70 = null;
        IToken char_literal71 = null;
        IToken char_literal73 = null;
        IToken STRING74 = null;
        IToken NUMBER75 = null;
        HScriptParser.expression_return expression72 = default(HScriptParser.expression_return);

        HScriptParser.syscall_return syscall76 = default(HScriptParser.syscall_return);

        HScriptParser.funccall_return funccall77 = default(HScriptParser.funccall_return);


        CommonTree IDENT70_tree=null;
        CommonTree char_literal71_tree=null;
        CommonTree char_literal73_tree=null;
        CommonTree STRING74_tree=null;
        CommonTree NUMBER75_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:221:2: ( IDENT | '(' expression ')' | STRING | NUMBER | syscall | funccall )
            int alt16 = 6;
            switch ( input.LA(1) ) 
            {
            case IDENT:
            	{
                int LA16_1 = input.LA(2);

                if ( (LA16_1 == 15) )
                {
                    alt16 = 6;
                }
                else if ( (LA16_1 == NEWLINE || LA16_1 == 12 || (LA16_1 >= 16 && LA16_1 <= 17) || (LA16_1 >= 25 && LA16_1 <= 36)) )
                {
                    alt16 = 1;
                }
                else 
                {
                    NoViableAltException nvae_d16s1 =
                        new NoViableAltException("", 16, 1, input);

                    throw nvae_d16s1;
                }
                }
                break;
            case 15:
            	{
                alt16 = 2;
                }
                break;
            case STRING:
            	{
                alt16 = 3;
                }
                break;
            case NUMBER:
            	{
                alt16 = 4;
                }
                break;
            case 23:
            	{
                alt16 = 5;
                }
                break;
            	default:
            	    NoViableAltException nvae_d16s0 =
            	        new NoViableAltException("", 16, 0, input);

            	    throw nvae_d16s0;
            }

            switch (alt16) 
            {
                case 1 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:221:4: IDENT
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	IDENT70=(IToken)Match(input,IDENT,FOLLOW_IDENT_in_term840); 
                    		IDENT70_tree = (CommonTree)adaptor.Create(IDENT70);
                    		adaptor.AddChild(root_0, IDENT70_tree);

                    	retval.n =  new HSVariable(IDENT70.Text);

                    }
                    break;
                case 2 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:222:4: '(' expression ')'
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	char_literal71=(IToken)Match(input,15,FOLLOW_15_in_term851); 
                    		char_literal71_tree = (CommonTree)adaptor.Create(char_literal71);
                    		adaptor.AddChild(root_0, char_literal71_tree);

                    	PushFollow(FOLLOW_expression_in_term853);
                    	expression72 = expression();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, expression72.Tree);
                    	char_literal73=(IToken)Match(input,16,FOLLOW_16_in_term855); 
                    		char_literal73_tree = (CommonTree)adaptor.Create(char_literal73);
                    		adaptor.AddChild(root_0, char_literal73_tree);

                    	retval.n =  ((expression72 != null) ? expression72.n : default(IHSNode));

                    }
                    break;
                case 3 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:223:4: STRING
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	STRING74=(IToken)Match(input,STRING,FOLLOW_STRING_in_term863); 
                    		STRING74_tree = (CommonTree)adaptor.Create(STRING74);
                    		adaptor.AddChild(root_0, STRING74_tree);

                    	retval.n =  new HSString(STRING74.Text);

                    }
                    break;
                case 4 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:224:4: NUMBER
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	NUMBER75=(IToken)Match(input,NUMBER,FOLLOW_NUMBER_in_term874); 
                    		NUMBER75_tree = (CommonTree)adaptor.Create(NUMBER75);
                    		adaptor.AddChild(root_0, NUMBER75_tree);

                    	retval.n =  new HSNumber(int.Parse(NUMBER75.Text));

                    }
                    break;
                case 5 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:225:4: syscall
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_syscall_in_term885);
                    	syscall76 = syscall();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, syscall76.Tree);
                    	retval.n =  ((syscall76 != null) ? syscall76.n : default(IHSNode));

                    }
                    break;
                case 6 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:226:4: funccall
                    {
                    	root_0 = (CommonTree)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_funccall_in_term896);
                    	funccall77 = funccall();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, funccall77.Tree);
                    	retval.n =  ((funccall77 != null) ? funccall77.n : default(IHSNode));

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "term"

    public class negation_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "negation"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:229:1: negation returns [IHSNode n] : ( '!' )* term ;
    public HScriptParser.negation_return negation() // throws RecognitionException [1]
    {   
        HScriptParser.negation_return retval = new HScriptParser.negation_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken char_literal78 = null;
        HScriptParser.term_return term79 = default(HScriptParser.term_return);


        CommonTree char_literal78_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:230:2: ( ( '!' )* term )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:230:4: ( '!' )* term
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	 bool neg = false; 
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:231:4: ( '!' )*
            	do 
            	{
            	    int alt17 = 2;
            	    int LA17_0 = input.LA(1);

            	    if ( (LA17_0 == 24) )
            	    {
            	        alt17 = 1;
            	    }


            	    switch (alt17) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:231:5: '!'
            			    {
            			    	char_literal78=(IToken)Match(input,24,FOLLOW_24_in_negation924); 
            			    		char_literal78_tree = (CommonTree)adaptor.Create(char_literal78);
            			    		adaptor.AddChild(root_0, char_literal78_tree);

            			    	neg = !neg;

            			    }
            			    break;

            			default:
            			    goto loop17;
            	    }
            	} while (true);

            	loop17:
            		;	// Stops C# compiler whining that label 'loop17' has no statements

            	PushFollow(FOLLOW_term_in_negation929);
            	term79 = term();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, term79.Tree);

            		  	if(neg == true)
            		  	{
            		  		retval.n =  new HSNegation(((term79 != null) ? term79.n : default(IHSNode)));
            		  	}
            		  	else
            		  	{
            		  		retval.n =  ((term79 != null) ? term79.n : default(IHSNode));
            		  	}
            		  

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "negation"

    public class unary_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "unary"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:244:1: unary returns [IHSNode n] : ( '+' | '-' )* negation ;
    public HScriptParser.unary_return unary() // throws RecognitionException [1]
    {   
        HScriptParser.unary_return retval = new HScriptParser.unary_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken char_literal80 = null;
        IToken char_literal81 = null;
        HScriptParser.negation_return negation82 = default(HScriptParser.negation_return);


        CommonTree char_literal80_tree=null;
        CommonTree char_literal81_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:245:2: ( ( '+' | '-' )* negation )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:245:4: ( '+' | '-' )* negation
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	bool negative = false;
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:246:2: ( '+' | '-' )*
            	do 
            	{
            	    int alt18 = 3;
            	    int LA18_0 = input.LA(1);

            	    if ( (LA18_0 == 25) )
            	    {
            	        alt18 = 1;
            	    }
            	    else if ( (LA18_0 == 26) )
            	    {
            	        alt18 = 2;
            	    }


            	    switch (alt18) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:246:3: '+'
            			    {
            			    	char_literal80=(IToken)Match(input,25,FOLLOW_25_in_unary955); 
            			    		char_literal80_tree = (CommonTree)adaptor.Create(char_literal80);
            			    		adaptor.AddChild(root_0, char_literal80_tree);


            			    }
            			    break;
            			case 2 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:246:9: '-'
            			    {
            			    	char_literal81=(IToken)Match(input,26,FOLLOW_26_in_unary959); 
            			    		char_literal81_tree = (CommonTree)adaptor.Create(char_literal81);
            			    		adaptor.AddChild(root_0, char_literal81_tree);

            			    	negative = !negative;

            			    }
            			    break;

            			default:
            			    goto loop18;
            	    }
            	} while (true);

            	loop18:
            		;	// Stops C# compiler whining that label 'loop18' has no statements

            	PushFollow(FOLLOW_negation_in_unary965);
            	negation82 = negation();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, negation82.Tree);

            			if(negative == true)
            			{
            				retval.n =  new HSNegative(((negation82 != null) ? negation82.n : default(IHSNode)));
            			}
            			else
            			{
            				retval.n =  ((negation82 != null) ? negation82.n : default(IHSNode));
            			}
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "unary"

    public class mult_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "mult"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:259:1: mult returns [IHSNode n] : op1= unary ( '*' op2= unary | '/' op2= unary )* ;
    public HScriptParser.mult_return mult() // throws RecognitionException [1]
    {   
        HScriptParser.mult_return retval = new HScriptParser.mult_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken char_literal83 = null;
        IToken char_literal84 = null;
        HScriptParser.unary_return op1 = default(HScriptParser.unary_return);

        HScriptParser.unary_return op2 = default(HScriptParser.unary_return);


        CommonTree char_literal83_tree=null;
        CommonTree char_literal84_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:260:2: (op1= unary ( '*' op2= unary | '/' op2= unary )* )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:260:4: op1= unary ( '*' op2= unary | '/' op2= unary )*
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_unary_in_mult986);
            	op1 = unary();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, op1.Tree);
            	 retval.n =  ((op1 != null) ? op1.n : default(IHSNode)); 
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:261:3: ( '*' op2= unary | '/' op2= unary )*
            	do 
            	{
            	    int alt19 = 3;
            	    int LA19_0 = input.LA(1);

            	    if ( (LA19_0 == 27) )
            	    {
            	        alt19 = 1;
            	    }
            	    else if ( (LA19_0 == 28) )
            	    {
            	        alt19 = 2;
            	    }


            	    switch (alt19) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:261:4: '*' op2= unary
            			    {
            			    	char_literal83=(IToken)Match(input,27,FOLLOW_27_in_mult996); 
            			    		char_literal83_tree = (CommonTree)adaptor.Create(char_literal83);
            			    		adaptor.AddChild(root_0, char_literal83_tree);

            			    	PushFollow(FOLLOW_unary_in_mult1000);
            			    	op2 = unary();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, op2.Tree);
            			    	 retval.n =  new HSMultiply(retval.n, ((op2 != null) ? op2.n : default(IHSNode))); 

            			    }
            			    break;
            			case 2 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:262:4: '/' op2= unary
            			    {
            			    	char_literal84=(IToken)Match(input,28,FOLLOW_28_in_mult1008); 
            			    		char_literal84_tree = (CommonTree)adaptor.Create(char_literal84);
            			    		adaptor.AddChild(root_0, char_literal84_tree);

            			    	PushFollow(FOLLOW_unary_in_mult1012);
            			    	op2 = unary();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, op2.Tree);
            			    	 retval.n =  new HSDivide(retval.n, ((op2 != null) ? op2.n : default(IHSNode))); 

            			    }
            			    break;

            			default:
            			    goto loop19;
            	    }
            	} while (true);

            	loop19:
            		;	// Stops C# compiler whining that label 'loop19' has no statements


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "mult"

    public class add_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "add"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:266:1: add returns [IHSNode n] : op1= mult ( '+' op2= mult | '-' op2= mult )* ;
    public HScriptParser.add_return add() // throws RecognitionException [1]
    {   
        HScriptParser.add_return retval = new HScriptParser.add_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken char_literal85 = null;
        IToken char_literal86 = null;
        HScriptParser.mult_return op1 = default(HScriptParser.mult_return);

        HScriptParser.mult_return op2 = default(HScriptParser.mult_return);


        CommonTree char_literal85_tree=null;
        CommonTree char_literal86_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:267:2: (op1= mult ( '+' op2= mult | '-' op2= mult )* )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:267:4: op1= mult ( '+' op2= mult | '-' op2= mult )*
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_mult_in_add1038);
            	op1 = mult();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, op1.Tree);
            	 retval.n =  ((op1 != null) ? op1.n : default(IHSNode)); 
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:268:3: ( '+' op2= mult | '-' op2= mult )*
            	do 
            	{
            	    int alt20 = 3;
            	    int LA20_0 = input.LA(1);

            	    if ( (LA20_0 == 25) )
            	    {
            	        alt20 = 1;
            	    }
            	    else if ( (LA20_0 == 26) )
            	    {
            	        alt20 = 2;
            	    }


            	    switch (alt20) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:268:4: '+' op2= mult
            			    {
            			    	char_literal85=(IToken)Match(input,25,FOLLOW_25_in_add1047); 
            			    		char_literal85_tree = (CommonTree)adaptor.Create(char_literal85);
            			    		adaptor.AddChild(root_0, char_literal85_tree);

            			    	PushFollow(FOLLOW_mult_in_add1051);
            			    	op2 = mult();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, op2.Tree);
            			    	 retval.n =  new HSAddition(retval.n, ((op2 != null) ? op2.n : default(IHSNode))); 

            			    }
            			    break;
            			case 2 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:269:4: '-' op2= mult
            			    {
            			    	char_literal86=(IToken)Match(input,26,FOLLOW_26_in_add1059); 
            			    		char_literal86_tree = (CommonTree)adaptor.Create(char_literal86);
            			    		adaptor.AddChild(root_0, char_literal86_tree);

            			    	PushFollow(FOLLOW_mult_in_add1063);
            			    	op2 = mult();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, op2.Tree);
            			    	 retval.n =  new HSSubtract(retval.n, ((op2 != null) ? op2.n : default(IHSNode))); 

            			    }
            			    break;

            			default:
            			    goto loop20;
            	    }
            	} while (true);

            	loop20:
            		;	// Stops C# compiler whining that label 'loop20' has no statements


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "add"

    public class relation_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "relation"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:273:1: relation returns [IHSNode n] : op1= add ( '==' op2= add | '!=' op2= add | '<' op2= add | '<=' op2= add | '>' op2= add | '>=' op2= add )* ;
    public HScriptParser.relation_return relation() // throws RecognitionException [1]
    {   
        HScriptParser.relation_return retval = new HScriptParser.relation_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken string_literal87 = null;
        IToken string_literal88 = null;
        IToken char_literal89 = null;
        IToken string_literal90 = null;
        IToken char_literal91 = null;
        IToken string_literal92 = null;
        HScriptParser.add_return op1 = default(HScriptParser.add_return);

        HScriptParser.add_return op2 = default(HScriptParser.add_return);


        CommonTree string_literal87_tree=null;
        CommonTree string_literal88_tree=null;
        CommonTree char_literal89_tree=null;
        CommonTree string_literal90_tree=null;
        CommonTree char_literal91_tree=null;
        CommonTree string_literal92_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:274:2: (op1= add ( '==' op2= add | '!=' op2= add | '<' op2= add | '<=' op2= add | '>' op2= add | '>=' op2= add )* )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:274:4: op1= add ( '==' op2= add | '!=' op2= add | '<' op2= add | '<=' op2= add | '>' op2= add | '>=' op2= add )*
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_add_in_relation1089);
            	op1 = add();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, op1.Tree);
            	 retval.n =  ((op1 != null) ? op1.n : default(IHSNode)); 
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:275:3: ( '==' op2= add | '!=' op2= add | '<' op2= add | '<=' op2= add | '>' op2= add | '>=' op2= add )*
            	do 
            	{
            	    int alt21 = 7;
            	    switch ( input.LA(1) ) 
            	    {
            	    case 29:
            	    	{
            	        alt21 = 1;
            	        }
            	        break;
            	    case 30:
            	    	{
            	        alt21 = 2;
            	        }
            	        break;
            	    case 31:
            	    	{
            	        alt21 = 3;
            	        }
            	        break;
            	    case 32:
            	    	{
            	        alt21 = 4;
            	        }
            	        break;
            	    case 33:
            	    	{
            	        alt21 = 5;
            	        }
            	        break;
            	    case 34:
            	    	{
            	        alt21 = 6;
            	        }
            	        break;

            	    }

            	    switch (alt21) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:275:4: '==' op2= add
            			    {
            			    	string_literal87=(IToken)Match(input,29,FOLLOW_29_in_relation1099); 
            			    		string_literal87_tree = (CommonTree)adaptor.Create(string_literal87);
            			    		adaptor.AddChild(root_0, string_literal87_tree);

            			    	PushFollow(FOLLOW_add_in_relation1103);
            			    	op2 = add();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, op2.Tree);
            			    	 retval.n =  new HSEqualTo(retval.n, ((op2 != null) ? op2.n : default(IHSNode))); 

            			    }
            			    break;
            			case 2 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:276:4: '!=' op2= add
            			    {
            			    	string_literal88=(IToken)Match(input,30,FOLLOW_30_in_relation1111); 
            			    		string_literal88_tree = (CommonTree)adaptor.Create(string_literal88);
            			    		adaptor.AddChild(root_0, string_literal88_tree);

            			    	PushFollow(FOLLOW_add_in_relation1115);
            			    	op2 = add();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, op2.Tree);
            			    	 retval.n =  new HSNotEqualTo(retval.n, ((op2 != null) ? op2.n : default(IHSNode))); 

            			    }
            			    break;
            			case 3 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:277:4: '<' op2= add
            			    {
            			    	char_literal89=(IToken)Match(input,31,FOLLOW_31_in_relation1123); 
            			    		char_literal89_tree = (CommonTree)adaptor.Create(char_literal89);
            			    		adaptor.AddChild(root_0, char_literal89_tree);

            			    	PushFollow(FOLLOW_add_in_relation1127);
            			    	op2 = add();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, op2.Tree);
            			    	 retval.n =  new HSLessThan(retval.n, ((op2 != null) ? op2.n : default(IHSNode))); 

            			    }
            			    break;
            			case 4 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:278:4: '<=' op2= add
            			    {
            			    	string_literal90=(IToken)Match(input,32,FOLLOW_32_in_relation1135); 
            			    		string_literal90_tree = (CommonTree)adaptor.Create(string_literal90);
            			    		adaptor.AddChild(root_0, string_literal90_tree);

            			    	PushFollow(FOLLOW_add_in_relation1138);
            			    	op2 = add();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, op2.Tree);
            			    	 retval.n =  new HSLessThanEqualTo(retval.n, ((op2 != null) ? op2.n : default(IHSNode))); 

            			    }
            			    break;
            			case 5 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:279:4: '>' op2= add
            			    {
            			    	char_literal91=(IToken)Match(input,33,FOLLOW_33_in_relation1146); 
            			    		char_literal91_tree = (CommonTree)adaptor.Create(char_literal91);
            			    		adaptor.AddChild(root_0, char_literal91_tree);

            			    	PushFollow(FOLLOW_add_in_relation1150);
            			    	op2 = add();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, op2.Tree);
            			    	 retval.n =  new HSMoreThan(retval.n, ((op2 != null) ? op2.n : default(IHSNode))); 

            			    }
            			    break;
            			case 6 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:280:4: '>=' op2= add
            			    {
            			    	string_literal92=(IToken)Match(input,34,FOLLOW_34_in_relation1158); 
            			    		string_literal92_tree = (CommonTree)adaptor.Create(string_literal92);
            			    		adaptor.AddChild(root_0, string_literal92_tree);

            			    	PushFollow(FOLLOW_add_in_relation1162);
            			    	op2 = add();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, op2.Tree);
            			    	 retval.n =  new HSMoreThanEqualTo(retval.n, ((op2 != null) ? op2.n : default(IHSNode))); 

            			    }
            			    break;

            			default:
            			    goto loop21;
            	    }
            	} while (true);

            	loop21:
            		;	// Stops C# compiler whining that label 'loop21' has no statements


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "relation"

    public class expression_return : ParserRuleReturnScope
    {
        public IHSNode n;
        private CommonTree tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (CommonTree) value; }
        }
    };

    // $ANTLR start "expression"
    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:284:1: expression returns [IHSNode n] : op1= relation ( '||' op2= relation | '&&' op2= relation )* ;
    public HScriptParser.expression_return expression() // throws RecognitionException [1]
    {   
        HScriptParser.expression_return retval = new HScriptParser.expression_return();
        retval.Start = input.LT(1);

        CommonTree root_0 = null;

        IToken string_literal93 = null;
        IToken string_literal94 = null;
        HScriptParser.relation_return op1 = default(HScriptParser.relation_return);

        HScriptParser.relation_return op2 = default(HScriptParser.relation_return);


        CommonTree string_literal93_tree=null;
        CommonTree string_literal94_tree=null;

        try 
    	{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:285:2: (op1= relation ( '||' op2= relation | '&&' op2= relation )* )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:285:4: op1= relation ( '||' op2= relation | '&&' op2= relation )*
            {
            	root_0 = (CommonTree)adaptor.GetNilNode();

            	PushFollow(FOLLOW_relation_in_expression1188);
            	op1 = relation();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, op1.Tree);
            	 retval.n =  ((op1 != null) ? op1.n : default(IHSNode)); 
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:286:3: ( '||' op2= relation | '&&' op2= relation )*
            	do 
            	{
            	    int alt22 = 3;
            	    int LA22_0 = input.LA(1);

            	    if ( (LA22_0 == 35) )
            	    {
            	        alt22 = 1;
            	    }
            	    else if ( (LA22_0 == 36) )
            	    {
            	        alt22 = 2;
            	    }


            	    switch (alt22) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:286:4: '||' op2= relation
            			    {
            			    	string_literal93=(IToken)Match(input,35,FOLLOW_35_in_expression1197); 
            			    		string_literal93_tree = (CommonTree)adaptor.Create(string_literal93);
            			    		adaptor.AddChild(root_0, string_literal93_tree);

            			    	PushFollow(FOLLOW_relation_in_expression1201);
            			    	op2 = relation();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, op2.Tree);
            			    	 retval.n =  new HSOr(retval.n, ((op2 != null) ? op2.n : default(IHSNode))); 

            			    }
            			    break;
            			case 2 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:287:4: '&&' op2= relation
            			    {
            			    	string_literal94=(IToken)Match(input,36,FOLLOW_36_in_expression1209); 
            			    		string_literal94_tree = (CommonTree)adaptor.Create(string_literal94);
            			    		adaptor.AddChild(root_0, string_literal94_tree);

            			    	PushFollow(FOLLOW_relation_in_expression1213);
            			    	op2 = relation();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, op2.Tree);
            			    	 retval.n =  new HSAnd(retval.n, ((op2 != null) ? op2.n : default(IHSNode))); 

            			    }
            			    break;

            			default:
            			    goto loop22;
            	    }
            	} while (true);

            	loop22:
            		;	// Stops C# compiler whining that label 'loop22' has no statements


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (CommonTree)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (CommonTree)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "expression"

    // Delegated rules


   	protected DFA8 dfa8;
	private void InitializeCyclicDFAs()
	{
    	this.dfa8 = new DFA8(this);
	}

    const string DFA8_eotS =
        "\x0a\uffff";
    const string DFA8_eofS =
        "\x0a\uffff";
    const string DFA8_minS =
        "\x01\x04\x01\uffff\x01\x0f\x07\uffff";
    const string DFA8_maxS =
        "\x01\x17\x01\uffff\x01\x13\x07\uffff";
    const string DFA8_acceptS =
        "\x01\uffff\x01\x01\x01\uffff\x01\x03\x01\x04\x01\x05\x01\x06\x01"+
        "\x08\x01\x02\x01\x07";
    const string DFA8_specialS =
        "\x0a\uffff}>";
    static readonly string[] DFA8_transitionS = {
            "\x01\x07\x01\x02\x07\uffff\x01\x01\x06\uffff\x01\x03\x01\x04"+
            "\x01\x05\x01\x06",
            "",
            "\x01\x09\x03\uffff\x01\x08",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static readonly short[] DFA8_eot = DFA.UnpackEncodedString(DFA8_eotS);
    static readonly short[] DFA8_eof = DFA.UnpackEncodedString(DFA8_eofS);
    static readonly char[] DFA8_min = DFA.UnpackEncodedStringToUnsignedChars(DFA8_minS);
    static readonly char[] DFA8_max = DFA.UnpackEncodedStringToUnsignedChars(DFA8_maxS);
    static readonly short[] DFA8_accept = DFA.UnpackEncodedString(DFA8_acceptS);
    static readonly short[] DFA8_special = DFA.UnpackEncodedString(DFA8_specialS);
    static readonly short[][] DFA8_transition = DFA.UnpackEncodedStringArray(DFA8_transitionS);

    protected class DFA8 : DFA
    {
        public DFA8(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = DFA8_eot;
            this.eof = DFA8_eof;
            this.min = DFA8_min;
            this.max = DFA8_max;
            this.accept = DFA8_accept;
            this.special = DFA8_special;
            this.transition = DFA8_transition;

        }

        override public string Description
        {
            get { return "83:1: statement returns [IHSNode n] : ( var_stmt | assign_stmt | if_stmt | while_stmt | return_stmt | syscall_stmt | funccall_stmt | NEWLINE );"; }
        }

    }

 

    public static readonly BitSet FOLLOW_NEWLINE_in_program64 = new BitSet(new ulong[]{0x0000000000006010UL});
    public static readonly BitSet FOLLOW_global_statement_in_program73 = new BitSet(new ulong[]{0x0000000000006010UL});
    public static readonly BitSet FOLLOW_NEWLINE_in_program85 = new BitSet(new ulong[]{0x0000000000000000UL});
    public static readonly BitSet FOLLOW_EOF_in_program88 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_global_var_stmt_in_global_statement106 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_func_stmt_in_global_statement115 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NEWLINE_in_global_statement124 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENT_in_parameters147 = new BitSet(new ulong[]{0x0000000000001002UL});
    public static readonly BitSet FOLLOW_12_in_parameters154 = new BitSet(new ulong[]{0x0000000000000020UL});
    public static readonly BitSet FOLLOW_IDENT_in_parameters158 = new BitSet(new ulong[]{0x0000000000001002UL});
    public static readonly BitSet FOLLOW_13_in_global_var_stmt185 = new BitSet(new ulong[]{0x0000000000000020UL});
    public static readonly BitSet FOLLOW_IDENT_in_global_var_stmt187 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_NEWLINE_in_global_var_stmt189 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_14_in_func_stmt210 = new BitSet(new ulong[]{0x0000000000000020UL});
    public static readonly BitSet FOLLOW_IDENT_in_func_stmt212 = new BitSet(new ulong[]{0x0000000000008000UL});
    public static readonly BitSet FOLLOW_15_in_func_stmt217 = new BitSet(new ulong[]{0x0000000000010020UL});
    public static readonly BitSet FOLLOW_parameters_in_func_stmt235 = new BitSet(new ulong[]{0x0000000000010000UL});
    public static readonly BitSet FOLLOW_16_in_func_stmt254 = new BitSet(new ulong[]{0x0000000000020000UL});
    public static readonly BitSet FOLLOW_17_in_func_stmt259 = new BitSet(new ulong[]{0x0000000000F42030UL});
    public static readonly BitSet FOLLOW_statement_in_func_stmt270 = new BitSet(new ulong[]{0x0000000000F42030UL});
    public static readonly BitSet FOLLOW_18_in_func_stmt287 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_NEWLINE_in_func_stmt289 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_var_stmt_in_statement309 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_assign_stmt_in_statement318 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_if_stmt_in_statement327 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_while_stmt_in_statement337 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_return_stmt_in_statement346 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_syscall_stmt_in_statement355 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_funccall_stmt_in_statement363 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NEWLINE_in_statement371 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_13_in_var_stmt391 = new BitSet(new ulong[]{0x0000000000000020UL});
    public static readonly BitSet FOLLOW_IDENT_in_var_stmt393 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_NEWLINE_in_var_stmt395 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_13_in_var_stmt406 = new BitSet(new ulong[]{0x0000000000000020UL});
    public static readonly BitSet FOLLOW_IDENT_in_var_stmt408 = new BitSet(new ulong[]{0x0000000000080000UL});
    public static readonly BitSet FOLLOW_19_in_var_stmt410 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_expression_in_var_stmt412 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_NEWLINE_in_var_stmt414 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENT_in_assign_stmt432 = new BitSet(new ulong[]{0x0000000000080000UL});
    public static readonly BitSet FOLLOW_19_in_assign_stmt434 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_expression_in_assign_stmt436 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_NEWLINE_in_assign_stmt438 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_20_in_if_stmt457 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_expression_in_if_stmt459 = new BitSet(new ulong[]{0x0000000000020000UL});
    public static readonly BitSet FOLLOW_17_in_if_stmt461 = new BitSet(new ulong[]{0x0000000000F42030UL});
    public static readonly BitSet FOLLOW_statement_in_if_stmt477 = new BitSet(new ulong[]{0x0000000000F42030UL});
    public static readonly BitSet FOLLOW_18_in_if_stmt496 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_NEWLINE_in_if_stmt498 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_21_in_while_stmt518 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_expression_in_while_stmt520 = new BitSet(new ulong[]{0x0000000000020000UL});
    public static readonly BitSet FOLLOW_17_in_while_stmt522 = new BitSet(new ulong[]{0x0000000000F42030UL});
    public static readonly BitSet FOLLOW_statement_in_while_stmt537 = new BitSet(new ulong[]{0x0000000000F42030UL});
    public static readonly BitSet FOLLOW_18_in_while_stmt557 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_NEWLINE_in_while_stmt559 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_22_in_return_stmt580 = new BitSet(new ulong[]{0x00000000078080F0UL});
    public static readonly BitSet FOLLOW_expression_in_return_stmt593 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_NEWLINE_in_return_stmt605 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_syscall_in_syscall_stmt626 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_NEWLINE_in_syscall_stmt628 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_23_in_syscall648 = new BitSet(new ulong[]{0x0000000000000020UL});
    public static readonly BitSet FOLLOW_IDENT_in_syscall650 = new BitSet(new ulong[]{0x0000000000008000UL});
    public static readonly BitSet FOLLOW_15_in_syscall654 = new BitSet(new ulong[]{0x00000000078180E0UL});
    public static readonly BitSet FOLLOW_args_in_syscall672 = new BitSet(new ulong[]{0x0000000000010000UL});
    public static readonly BitSet FOLLOW_16_in_syscall691 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_funccall_in_funccall_stmt712 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_NEWLINE_in_funccall_stmt714 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IDENT_in_funccall734 = new BitSet(new ulong[]{0x0000000000008000UL});
    public static readonly BitSet FOLLOW_15_in_funccall739 = new BitSet(new ulong[]{0x00000000078180E0UL});
    public static readonly BitSet FOLLOW_args_in_funccall755 = new BitSet(new ulong[]{0x0000000000010000UL});
    public static readonly BitSet FOLLOW_16_in_funccall772 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_expression_in_args794 = new BitSet(new ulong[]{0x0000000000001002UL});
    public static readonly BitSet FOLLOW_12_in_args804 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_expression_in_args811 = new BitSet(new ulong[]{0x0000000000001002UL});
    public static readonly BitSet FOLLOW_IDENT_in_term840 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_15_in_term851 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_expression_in_term853 = new BitSet(new ulong[]{0x0000000000010000UL});
    public static readonly BitSet FOLLOW_16_in_term855 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_STRING_in_term863 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_NUMBER_in_term874 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_syscall_in_term885 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_funccall_in_term896 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_24_in_negation924 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_term_in_negation929 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_25_in_unary955 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_26_in_unary959 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_negation_in_unary965 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_unary_in_mult986 = new BitSet(new ulong[]{0x0000000018000002UL});
    public static readonly BitSet FOLLOW_27_in_mult996 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_unary_in_mult1000 = new BitSet(new ulong[]{0x0000000018000002UL});
    public static readonly BitSet FOLLOW_28_in_mult1008 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_unary_in_mult1012 = new BitSet(new ulong[]{0x0000000018000002UL});
    public static readonly BitSet FOLLOW_mult_in_add1038 = new BitSet(new ulong[]{0x0000000006000002UL});
    public static readonly BitSet FOLLOW_25_in_add1047 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_mult_in_add1051 = new BitSet(new ulong[]{0x0000000006000002UL});
    public static readonly BitSet FOLLOW_26_in_add1059 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_mult_in_add1063 = new BitSet(new ulong[]{0x0000000006000002UL});
    public static readonly BitSet FOLLOW_add_in_relation1089 = new BitSet(new ulong[]{0x00000007E0000002UL});
    public static readonly BitSet FOLLOW_29_in_relation1099 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_add_in_relation1103 = new BitSet(new ulong[]{0x00000007E0000002UL});
    public static readonly BitSet FOLLOW_30_in_relation1111 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_add_in_relation1115 = new BitSet(new ulong[]{0x00000007E0000002UL});
    public static readonly BitSet FOLLOW_31_in_relation1123 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_add_in_relation1127 = new BitSet(new ulong[]{0x00000007E0000002UL});
    public static readonly BitSet FOLLOW_32_in_relation1135 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_add_in_relation1138 = new BitSet(new ulong[]{0x00000007E0000002UL});
    public static readonly BitSet FOLLOW_33_in_relation1146 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_add_in_relation1150 = new BitSet(new ulong[]{0x00000007E0000002UL});
    public static readonly BitSet FOLLOW_34_in_relation1158 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_add_in_relation1162 = new BitSet(new ulong[]{0x00000007E0000002UL});
    public static readonly BitSet FOLLOW_relation_in_expression1188 = new BitSet(new ulong[]{0x0000001800000002UL});
    public static readonly BitSet FOLLOW_35_in_expression1197 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_relation_in_expression1201 = new BitSet(new ulong[]{0x0000001800000002UL});
    public static readonly BitSet FOLLOW_36_in_expression1209 = new BitSet(new ulong[]{0x00000000078080E0UL});
    public static readonly BitSet FOLLOW_relation_in_expression1213 = new BitSet(new ulong[]{0x0000001800000002UL});

}
}