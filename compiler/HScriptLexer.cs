// $ANTLR 3.2 Sep 23, 2009 12:02:23 /home/habeel/Programming/Projects/nettest/hscript/HScript.g 2010-08-29 19:29:10

// The variable 'variable' is assigned but its value is never used.
#pragma warning disable 168, 219
// Unreachable code detected.
#pragma warning disable 162


using System;
using Antlr.Runtime;
using IList 		= System.Collections.IList;
using ArrayList 	= System.Collections.ArrayList;
using Stack 		= Antlr.Runtime.Collections.StackList;


namespace hscript.Compiler
{
public partial class HScriptLexer : Lexer {
    public const int T__29 = 29;
    public const int T__28 = 28;
    public const int T__27 = 27;
    public const int T__26 = 26;
    public const int T__25 = 25;
    public const int T__24 = 24;
    public const int T__23 = 23;
    public const int T__22 = 22;
    public const int T__21 = 21;
    public const int T__20 = 20;
    public const int NUMBER = 7;
    public const int EOF = -1;
    public const int T__30 = 30;
    public const int T__19 = 19;
    public const int T__31 = 31;
    public const int T__32 = 32;
    public const int T__16 = 16;
    public const int T__33 = 33;
    public const int STRING_CHAR = 8;
    public const int ESC_SEQ = 9;
    public const int WS = 11;
    public const int T__15 = 15;
    public const int T__34 = 34;
    public const int T__18 = 18;
    public const int T__35 = 35;
    public const int NEWLINE = 4;
    public const int T__17 = 17;
    public const int T__36 = 36;
    public const int T__12 = 12;
    public const int T__14 = 14;
    public const int T__13 = 13;
    public const int IDENT = 5;
    public const int COMMENT = 10;
    public const int STRING = 6;

    // delegates
    // delegators

    public HScriptLexer() 
    {
		InitializeCyclicDFAs();
    }
    public HScriptLexer(ICharStream input)
		: this(input, null) {
    }
    public HScriptLexer(ICharStream input, RecognizerSharedState state)
		: base(input, state) {
		InitializeCyclicDFAs(); 

    }
    
    override public string GrammarFileName
    {
    	get { return "/home/habeel/Programming/Projects/nettest/hscript/HScript.g";} 
    }

    // $ANTLR start "T__12"
    public void mT__12() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__12;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:9:7: ( ',' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:9:9: ','
            {
            	Match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public void mT__13() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__13;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:10:7: ( 'var' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:10:9: 'var'
            {
            	Match("var"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public void mT__14() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__14;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:11:7: ( 'func' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:11:9: 'func'
            {
            	Match("func"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public void mT__15() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__15;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:12:7: ( '(' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:12:9: '('
            {
            	Match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public void mT__16() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__16;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:13:7: ( ')' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:13:9: ')'
            {
            	Match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public void mT__17() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__17;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:14:7: ( '{' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:14:9: '{'
            {
            	Match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public void mT__18() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__18;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:15:7: ( '}' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:15:9: '}'
            {
            	Match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public void mT__19() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__19;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:16:7: ( '=' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:16:9: '='
            {
            	Match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public void mT__20() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__20;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:17:7: ( 'if' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:17:9: 'if'
            {
            	Match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public void mT__21() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__21;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:18:7: ( 'while' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:18:9: 'while'
            {
            	Match("while"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public void mT__22() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__22;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:19:7: ( 'return' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:19:9: 'return'
            {
            	Match("return"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public void mT__23() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__23;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:20:7: ( '$ys.' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:20:9: '$ys.'
            {
            	Match("$ys."); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public void mT__24() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__24;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:21:7: ( '!' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:21:9: '!'
            {
            	Match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public void mT__25() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__25;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:22:7: ( '+' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:22:9: '+'
            {
            	Match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public void mT__26() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__26;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:23:7: ( '-' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:23:9: '-'
            {
            	Match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public void mT__27() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__27;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:24:7: ( '*' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:24:9: '*'
            {
            	Match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public void mT__28() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__28;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:25:7: ( '/' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:25:9: '/'
            {
            	Match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public void mT__29() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__29;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:26:7: ( '==' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:26:9: '=='
            {
            	Match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public void mT__30() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__30;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:27:7: ( '!=' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:27:9: '!='
            {
            	Match("!="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public void mT__31() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__31;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:28:7: ( '<' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:28:9: '<'
            {
            	Match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public void mT__32() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__32;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:29:7: ( '<=' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:29:9: '<='
            {
            	Match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public void mT__33() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__33;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:30:7: ( '>' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:30:9: '>'
            {
            	Match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public void mT__34() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__34;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:31:7: ( '>=' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:31:9: '>='
            {
            	Match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public void mT__35() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__35;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:32:7: ( '||' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:32:9: '||'
            {
            	Match("||"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public void mT__36() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = T__36;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:33:7: ( '&&' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:33:9: '&&'
            {
            	Match("&&"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "IDENT"
    public void mIDENT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = IDENT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:292:2: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* ( '.' ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )* )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:292:4: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* ( '.' ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )*
            {
            	if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z') || input.LA(1) == '_' || (input.LA(1) >= 'a' && input.LA(1) <= 'z') ) 
            	{
            	    input.Consume();

            	}
            	else 
            	{
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    Recover(mse);
            	    throw mse;}

            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:292:28: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
            	do 
            	{
            	    int alt1 = 2;
            	    int LA1_0 = input.LA(1);

            	    if ( ((LA1_0 >= '0' && LA1_0 <= '9') || (LA1_0 >= 'A' && LA1_0 <= 'Z') || LA1_0 == '_' || (LA1_0 >= 'a' && LA1_0 <= 'z')) )
            	    {
            	        alt1 = 1;
            	    }


            	    switch (alt1) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:
            			    {
            			    	if ( (input.LA(1) >= '0' && input.LA(1) <= '9') || (input.LA(1) >= 'A' && input.LA(1) <= 'Z') || input.LA(1) == '_' || (input.LA(1) >= 'a' && input.LA(1) <= 'z') ) 
            			    	{
            			    	    input.Consume();

            			    	}
            			    	else 
            			    	{
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    Recover(mse);
            			    	    throw mse;}


            			    }
            			    break;

            			default:
            			    goto loop1;
            	    }
            	} while (true);

            	loop1:
            		;	// Stops C# compiler whining that label 'loop1' has no statements

            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:292:62: ( '.' ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )*
            	do 
            	{
            	    int alt3 = 2;
            	    int LA3_0 = input.LA(1);

            	    if ( (LA3_0 == '.') )
            	    {
            	        alt3 = 1;
            	    }


            	    switch (alt3) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:292:63: '.' ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
            			    {
            			    	Match('.'); 
            			    	if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z') || input.LA(1) == '_' || (input.LA(1) >= 'a' && input.LA(1) <= 'z') ) 
            			    	{
            			    	    input.Consume();

            			    	}
            			    	else 
            			    	{
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    Recover(mse);
            			    	    throw mse;}

            			    	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:292:91: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
            			    	do 
            			    	{
            			    	    int alt2 = 2;
            			    	    int LA2_0 = input.LA(1);

            			    	    if ( ((LA2_0 >= '0' && LA2_0 <= '9') || (LA2_0 >= 'A' && LA2_0 <= 'Z') || LA2_0 == '_' || (LA2_0 >= 'a' && LA2_0 <= 'z')) )
            			    	    {
            			    	        alt2 = 1;
            			    	    }


            			    	    switch (alt2) 
            			    		{
            			    			case 1 :
            			    			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:
            			    			    {
            			    			    	if ( (input.LA(1) >= '0' && input.LA(1) <= '9') || (input.LA(1) >= 'A' && input.LA(1) <= 'Z') || input.LA(1) == '_' || (input.LA(1) >= 'a' && input.LA(1) <= 'z') ) 
            			    			    	{
            			    			    	    input.Consume();

            			    			    	}
            			    			    	else 
            			    			    	{
            			    			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    			    	    Recover(mse);
            			    			    	    throw mse;}


            			    			    }
            			    			    break;

            			    			default:
            			    			    goto loop2;
            			    	    }
            			    	} while (true);

            			    	loop2:
            			    		;	// Stops C# compiler whining that label 'loop2' has no statements


            			    }
            			    break;

            			default:
            			    goto loop3;
            	    }
            	} while (true);

            	loop3:
            		;	// Stops C# compiler whining that label 'loop3' has no statements


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "IDENT"

    // $ANTLR start "NUMBER"
    public void mNUMBER() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = NUMBER;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:296:2: ( ( '0' .. '9' )+ )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:296:4: ( '0' .. '9' )+
            {
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:296:4: ( '0' .. '9' )+
            	int cnt4 = 0;
            	do 
            	{
            	    int alt4 = 2;
            	    int LA4_0 = input.LA(1);

            	    if ( ((LA4_0 >= '0' && LA4_0 <= '9')) )
            	    {
            	        alt4 = 1;
            	    }


            	    switch (alt4) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:296:4: '0' .. '9'
            			    {
            			    	MatchRange('0','9'); 

            			    }
            			    break;

            			default:
            			    if ( cnt4 >= 1 ) goto loop4;
            		            EarlyExitException eee4 =
            		                new EarlyExitException(4, input);
            		            throw eee4;
            	    }
            	    cnt4++;
            	} while (true);

            	loop4:
            		;	// Stops C# compiler whining that label 'loop4' has no statements


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "NUMBER"

    // $ANTLR start "STRING"
    public void mSTRING() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = STRING;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:300:2: ( '\"' ( STRING_CHAR )* '\"' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:300:4: '\"' ( STRING_CHAR )* '\"'
            {
            	Match('\"'); 
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:300:8: ( STRING_CHAR )*
            	do 
            	{
            	    int alt5 = 2;
            	    int LA5_0 = input.LA(1);

            	    if ( ((LA5_0 >= '\u0000' && LA5_0 <= '\t') || (LA5_0 >= '\u000B' && LA5_0 <= '!') || (LA5_0 >= '#' && LA5_0 <= '\uFFFF')) )
            	    {
            	        alt5 = 1;
            	    }


            	    switch (alt5) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:300:8: STRING_CHAR
            			    {
            			    	mSTRING_CHAR(); 

            			    }
            			    break;

            			default:
            			    goto loop5;
            	    }
            	} while (true);

            	loop5:
            		;	// Stops C# compiler whining that label 'loop5' has no statements

            	Match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "STRING"

    // $ANTLR start "STRING_CHAR"
    public void mSTRING_CHAR() // throws RecognitionException [2]
    {
    		try
    		{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:305:2: ( ESC_SEQ | ~ ( '\"' | '\\n' ) )
            int alt6 = 2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0 == '\\') )
            {
                int LA6_1 = input.LA(2);

                if ( (LA6_1 == '\"' || LA6_1 == '\\' || LA6_1 == 'b' || LA6_1 == 'f' || LA6_1 == 'n' || LA6_1 == 'r' || LA6_1 == 't') )
                {
                    alt6 = 1;
                }
                else 
                {
                    alt6 = 2;}
            }
            else if ( ((LA6_0 >= '\u0000' && LA6_0 <= '\t') || (LA6_0 >= '\u000B' && LA6_0 <= '!') || (LA6_0 >= '#' && LA6_0 <= '[') || (LA6_0 >= ']' && LA6_0 <= '\uFFFF')) )
            {
                alt6 = 2;
            }
            else 
            {
                NoViableAltException nvae_d6s0 =
                    new NoViableAltException("", 6, 0, input);

                throw nvae_d6s0;
            }
            switch (alt6) 
            {
                case 1 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:305:4: ESC_SEQ
                    {
                    	mESC_SEQ(); 

                    }
                    break;
                case 2 :
                    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:305:14: ~ ( '\"' | '\\n' )
                    {
                    	if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t') || (input.LA(1) >= '\u000B' && input.LA(1) <= '!') || (input.LA(1) >= '#' && input.LA(1) <= '\uFFFF') ) 
                    	{
                    	    input.Consume();

                    	}
                    	else 
                    	{
                    	    MismatchedSetException mse = new MismatchedSetException(null,input);
                    	    Recover(mse);
                    	    throw mse;}


                    }
                    break;

            }
        }
        finally 
    	{
        }
    }
    // $ANTLR end "STRING_CHAR"

    // $ANTLR start "ESC_SEQ"
    public void mESC_SEQ() // throws RecognitionException [2]
    {
    		try
    		{
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:310:2: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\"' | '\\\\' ) )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:310:4: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\"' | '\\\\' )
            {
            	Match('\\'); 
            	if ( input.LA(1) == '\"' || input.LA(1) == '\\' || input.LA(1) == 'b' || input.LA(1) == 'f' || input.LA(1) == 'n' || input.LA(1) == 'r' || input.LA(1) == 't' ) 
            	{
            	    input.Consume();

            	}
            	else 
            	{
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    Recover(mse);
            	    throw mse;}


            }

        }
        finally 
    	{
        }
    }
    // $ANTLR end "ESC_SEQ"

    // $ANTLR start "NEWLINE"
    public void mNEWLINE() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = NEWLINE;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:314:2: ( ( ( '\\r' )? '\\n' )+ )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:314:4: ( ( '\\r' )? '\\n' )+
            {
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:314:4: ( ( '\\r' )? '\\n' )+
            	int cnt8 = 0;
            	do 
            	{
            	    int alt8 = 2;
            	    int LA8_0 = input.LA(1);

            	    if ( (LA8_0 == '\n' || LA8_0 == '\r') )
            	    {
            	        alt8 = 1;
            	    }


            	    switch (alt8) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:314:5: ( '\\r' )? '\\n'
            			    {
            			    	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:314:5: ( '\\r' )?
            			    	int alt7 = 2;
            			    	int LA7_0 = input.LA(1);

            			    	if ( (LA7_0 == '\r') )
            			    	{
            			    	    alt7 = 1;
            			    	}
            			    	switch (alt7) 
            			    	{
            			    	    case 1 :
            			    	        // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:314:5: '\\r'
            			    	        {
            			    	        	Match('\r'); 

            			    	        }
            			    	        break;

            			    	}

            			    	Match('\n'); 

            			    }
            			    break;

            			default:
            			    if ( cnt8 >= 1 ) goto loop8;
            		            EarlyExitException eee8 =
            		                new EarlyExitException(8, input);
            		            throw eee8;
            	    }
            	    cnt8++;
            	} while (true);

            	loop8:
            		;	// Stops C# compiler whining that label 'loop8' has no statements


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "NEWLINE"

    // $ANTLR start "COMMENT"
    public void mCOMMENT() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = COMMENT;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:318:2: ( '#' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:318:4: '#' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
            {
            	Match('#'); 
            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:318:8: (~ ( '\\n' | '\\r' ) )*
            	do 
            	{
            	    int alt9 = 2;
            	    int LA9_0 = input.LA(1);

            	    if ( ((LA9_0 >= '\u0000' && LA9_0 <= '\t') || (LA9_0 >= '\u000B' && LA9_0 <= '\f') || (LA9_0 >= '\u000E' && LA9_0 <= '\uFFFF')) )
            	    {
            	        alt9 = 1;
            	    }


            	    switch (alt9) 
            		{
            			case 1 :
            			    // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:318:8: ~ ( '\\n' | '\\r' )
            			    {
            			    	if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t') || (input.LA(1) >= '\u000B' && input.LA(1) <= '\f') || (input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) 
            			    	{
            			    	    input.Consume();

            			    	}
            			    	else 
            			    	{
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    Recover(mse);
            			    	    throw mse;}


            			    }
            			    break;

            			default:
            			    goto loop9;
            	    }
            	} while (true);

            	loop9:
            		;	// Stops C# compiler whining that label 'loop9' has no statements

            	// /home/habeel/Programming/Projects/nettest/hscript/HScript.g:318:22: ( '\\r' )?
            	int alt10 = 2;
            	int LA10_0 = input.LA(1);

            	if ( (LA10_0 == '\r') )
            	{
            	    alt10 = 1;
            	}
            	switch (alt10) 
            	{
            	    case 1 :
            	        // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:318:22: '\\r'
            	        {
            	        	Match('\r'); 

            	        }
            	        break;

            	}

            	Match('\n'); 
            	_channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "COMMENT"

    // $ANTLR start "WS"
    public void mWS() // throws RecognitionException [2]
    {
    		try
    		{
            int _type = WS;
    	int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:322:2: ( ( ' ' | '\\t' ) )
            // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:322:4: ( ' ' | '\\t' )
            {
            	if ( input.LA(1) == '\t' || input.LA(1) == ' ' ) 
            	{
            	    input.Consume();

            	}
            	else 
            	{
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    Recover(mse);
            	    throw mse;}

            	_channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally 
    	{
        }
    }
    // $ANTLR end "WS"

    override public void mTokens() // throws RecognitionException 
    {
        // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:8: ( T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | IDENT | NUMBER | STRING | NEWLINE | COMMENT | WS )
        int alt11 = 31;
        alt11 = dfa11.Predict(input);
        switch (alt11) 
        {
            case 1 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:10: T__12
                {
                	mT__12(); 

                }
                break;
            case 2 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:16: T__13
                {
                	mT__13(); 

                }
                break;
            case 3 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:22: T__14
                {
                	mT__14(); 

                }
                break;
            case 4 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:28: T__15
                {
                	mT__15(); 

                }
                break;
            case 5 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:34: T__16
                {
                	mT__16(); 

                }
                break;
            case 6 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:40: T__17
                {
                	mT__17(); 

                }
                break;
            case 7 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:46: T__18
                {
                	mT__18(); 

                }
                break;
            case 8 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:52: T__19
                {
                	mT__19(); 

                }
                break;
            case 9 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:58: T__20
                {
                	mT__20(); 

                }
                break;
            case 10 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:64: T__21
                {
                	mT__21(); 

                }
                break;
            case 11 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:70: T__22
                {
                	mT__22(); 

                }
                break;
            case 12 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:76: T__23
                {
                	mT__23(); 

                }
                break;
            case 13 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:82: T__24
                {
                	mT__24(); 

                }
                break;
            case 14 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:88: T__25
                {
                	mT__25(); 

                }
                break;
            case 15 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:94: T__26
                {
                	mT__26(); 

                }
                break;
            case 16 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:100: T__27
                {
                	mT__27(); 

                }
                break;
            case 17 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:106: T__28
                {
                	mT__28(); 

                }
                break;
            case 18 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:112: T__29
                {
                	mT__29(); 

                }
                break;
            case 19 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:118: T__30
                {
                	mT__30(); 

                }
                break;
            case 20 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:124: T__31
                {
                	mT__31(); 

                }
                break;
            case 21 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:130: T__32
                {
                	mT__32(); 

                }
                break;
            case 22 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:136: T__33
                {
                	mT__33(); 

                }
                break;
            case 23 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:142: T__34
                {
                	mT__34(); 

                }
                break;
            case 24 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:148: T__35
                {
                	mT__35(); 

                }
                break;
            case 25 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:154: T__36
                {
                	mT__36(); 

                }
                break;
            case 26 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:160: IDENT
                {
                	mIDENT(); 

                }
                break;
            case 27 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:166: NUMBER
                {
                	mNUMBER(); 

                }
                break;
            case 28 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:173: STRING
                {
                	mSTRING(); 

                }
                break;
            case 29 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:180: NEWLINE
                {
                	mNEWLINE(); 

                }
                break;
            case 30 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:188: COMMENT
                {
                	mCOMMENT(); 

                }
                break;
            case 31 :
                // /home/habeel/Programming/Projects/nettest/hscript/HScript.g:1:196: WS
                {
                	mWS(); 

                }
                break;

        }

    }


    protected DFA11 dfa11;
	private void InitializeCyclicDFAs()
	{
	    this.dfa11 = new DFA11(this);
	}

    const string DFA11_eotS =
        "\x02\uffff\x02\x16\x04\uffff\x01\x1f\x03\x16\x01\uffff\x01\x24\x04"+
        "\uffff\x01\x26\x01\x28\x08\uffff\x02\x16\x02\uffff\x01\x2b\x02\x16"+
        "\x06\uffff\x01\x2e\x01\x16\x01\uffff\x02\x16\x01\uffff\x01\x32\x02"+
        "\x16\x01\uffff\x01\x35\x01\x16\x01\uffff\x01\x37\x01\uffff";
    const string DFA11_eofS =
        "\x38\uffff";
    const string DFA11_minS =
        "\x01\x09\x01\uffff\x01\x61\x01\x75\x04\uffff\x01\x3d\x01\x66\x01"+
        "\x68\x01\x65\x01\uffff\x01\x3d\x04\uffff\x02\x3d\x08\uffff\x01\x72"+
        "\x01\x6e\x02\uffff\x01\x2e\x01\x69\x01\x74\x06\uffff\x01\x2e\x01"+
        "\x63\x01\uffff\x01\x6c\x01\x75\x01\uffff\x01\x2e\x01\x65\x01\x72"+
        "\x01\uffff\x01\x2e\x01\x6e\x01\uffff\x01\x2e\x01\uffff";
    const string DFA11_maxS =
        "\x01\x7d\x01\uffff\x01\x61\x01\x75\x04\uffff\x01\x3d\x01\x66\x01"+
        "\x68\x01\x65\x01\uffff\x01\x3d\x04\uffff\x02\x3d\x08\uffff\x01\x72"+
        "\x01\x6e\x02\uffff\x01\x7a\x01\x69\x01\x74\x06\uffff\x01\x7a\x01"+
        "\x63\x01\uffff\x01\x6c\x01\x75\x01\uffff\x01\x7a\x01\x65\x01\x72"+
        "\x01\uffff\x01\x7a\x01\x6e\x01\uffff\x01\x7a\x01\uffff";
    const string DFA11_acceptS =
        "\x01\uffff\x01\x01\x02\uffff\x01\x04\x01\x05\x01\x06\x01\x07\x04"+
        "\uffff\x01\x0c\x01\uffff\x01\x0e\x01\x0f\x01\x10\x01\x11\x02\uffff"+
        "\x01\x18\x01\x19\x01\x1a\x01\x1b\x01\x1c\x01\x1d\x01\x1e\x01\x1f"+
        "\x02\uffff\x01\x12\x01\x08\x03\uffff\x01\x13\x01\x0d\x01\x15\x01"+
        "\x14\x01\x17\x01\x16\x02\uffff\x01\x09\x02\uffff\x01\x02\x03\uffff"+
        "\x01\x03\x02\uffff\x01\x0a\x01\uffff\x01\x0b";
    const string DFA11_specialS =
        "\x38\uffff}>";
    static readonly string[] DFA11_transitionS = {
            "\x01\x1b\x01\x19\x02\uffff\x01\x19\x12\uffff\x01\x1b\x01\x0d"+
            "\x01\x18\x01\x1a\x01\x0c\x01\uffff\x01\x15\x01\uffff\x01\x04"+
            "\x01\x05\x01\x10\x01\x0e\x01\x01\x01\x0f\x01\uffff\x01\x11\x0a"+
            "\x17\x02\uffff\x01\x12\x01\x08\x01\x13\x02\uffff\x1a\x16\x04"+
            "\uffff\x01\x16\x01\uffff\x05\x16\x01\x03\x02\x16\x01\x09\x08"+
            "\x16\x01\x0b\x03\x16\x01\x02\x01\x0a\x03\x16\x01\x06\x01\x14"+
            "\x01\x07",
            "",
            "\x01\x1c",
            "\x01\x1d",
            "",
            "",
            "",
            "",
            "\x01\x1e",
            "\x01\x20",
            "\x01\x21",
            "\x01\x22",
            "",
            "\x01\x23",
            "",
            "",
            "",
            "",
            "\x01\x25",
            "\x01\x27",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\x01\x29",
            "\x01\x2a",
            "",
            "",
            "\x01\x16\x01\uffff\x0a\x16\x07\uffff\x1a\x16\x04\uffff\x01"+
            "\x16\x01\uffff\x1a\x16",
            "\x01\x2c",
            "\x01\x2d",
            "",
            "",
            "",
            "",
            "",
            "",
            "\x01\x16\x01\uffff\x0a\x16\x07\uffff\x1a\x16\x04\uffff\x01"+
            "\x16\x01\uffff\x1a\x16",
            "\x01\x2f",
            "",
            "\x01\x30",
            "\x01\x31",
            "",
            "\x01\x16\x01\uffff\x0a\x16\x07\uffff\x1a\x16\x04\uffff\x01"+
            "\x16\x01\uffff\x1a\x16",
            "\x01\x33",
            "\x01\x34",
            "",
            "\x01\x16\x01\uffff\x0a\x16\x07\uffff\x1a\x16\x04\uffff\x01"+
            "\x16\x01\uffff\x1a\x16",
            "\x01\x36",
            "",
            "\x01\x16\x01\uffff\x0a\x16\x07\uffff\x1a\x16\x04\uffff\x01"+
            "\x16\x01\uffff\x1a\x16",
            ""
    };

    static readonly short[] DFA11_eot = DFA.UnpackEncodedString(DFA11_eotS);
    static readonly short[] DFA11_eof = DFA.UnpackEncodedString(DFA11_eofS);
    static readonly char[] DFA11_min = DFA.UnpackEncodedStringToUnsignedChars(DFA11_minS);
    static readonly char[] DFA11_max = DFA.UnpackEncodedStringToUnsignedChars(DFA11_maxS);
    static readonly short[] DFA11_accept = DFA.UnpackEncodedString(DFA11_acceptS);
    static readonly short[] DFA11_special = DFA.UnpackEncodedString(DFA11_specialS);
    static readonly short[][] DFA11_transition = DFA.UnpackEncodedStringArray(DFA11_transitionS);

    protected class DFA11 : DFA
    {
        public DFA11(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 11;
            this.eot = DFA11_eot;
            this.eof = DFA11_eof;
            this.min = DFA11_min;
            this.max = DFA11_max;
            this.accept = DFA11_accept;
            this.special = DFA11_special;
            this.transition = DFA11_transition;

        }

        override public string Description
        {
            get { return "1:1: Tokens : ( T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | IDENT | NUMBER | STRING | NEWLINE | COMMENT | WS );"; }
        }

    }

 
    
}
}