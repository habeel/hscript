load system

func main() {
     var thread1 = $ys.ThreadCreate(t1)
     var thread2 = $ys.ThreadCreate(t2)
     $ys.ThreadResume(thread1)
     $ys.ThreadResume(thread2)
}

func t1() {
     while(1) {
     	      Console.Write(".")
	      Sleep(100)
     }
}

func t2() {
     var vari = 0
     while(1) {
     	      Console.Write(",")
	      Sleep(200 + vari)
	      vari = vari + 20
	      if(vari >= 200) {
	      	      vari = 0
	      }
     }
}
