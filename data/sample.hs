load system
load thread
load math

func process() {
	var r = random()
	var i = 0
	var max = 50
	
	while(i < max) {
		println(i + ": " + r)
		i = i + 1
		# sleep(mod(random(),2000))
		sleep(50)
	}
}

func main() {
	var i = 0
	
	while (i < 1000) {
		var thread = createthread("process")
		runthread(thread)
		i = i + 1
	}
}
