## Setup ##

**Create a lib directory**

$HOME/lib/hscript

Place all libraries that you want to share in that directory. You can require them inside the script by saying


```
#!hscript

load system
```

etc.

Or you can just place the other files you want to load in the same directory as the script.

## Samples ##

Sample scripts can be found in the "data" folder.
