using System;
using System.Threading;
using System.IO;
using hscript.Compiler;
using hscript.VirtualMachine;

namespace hscript
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			if(args.Length == 0)
			{
				HSCompiler compiler = new HSCompiler();
//				compiler.Load(args[0]);
				compiler.Load("../../data/sample.hs");
				string asm = compiler.Compile();
				HSVirtualMachine vm = new HSVirtualMachine();
				
				// register libraries
				hscript.VirtualMachine.Library.HSConsole console = new hscript.VirtualMachine.Library.HSConsole();
				console.Register(vm);
				
				hscript.VirtualMachine.Library.HSThreading threading = new hscript.VirtualMachine.Library.HSThreading();
				threading.Register(vm);
				
				hscript.VirtualMachine.Library.HSMath math = new hscript.VirtualMachine.Library.HSMath();
				math.Register(vm);
				
				vm.Load(asm);
				vm.Init();
				vm.Run();
			}
			else
			{
				Console.WriteLine("HScript v1");
				Console.WriteLine();
				Console.WriteLine("Usage: hscript [filename]");
			}
		}
	}
}
