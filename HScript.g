grammar HScript;

options
{
	language=CSharp2;
	output=AST;
	ASTLabelType=CommonTree;
}

@lexer::namespace {hscript.Compiler}
@parser::namespace {hscript.Compiler}

@header {
	using hscript.Compiler.Nodes;
}

program returns [IHSNode n]
	: { HSProgram tmpp = new HSProgram(); }
	NEWLINE? 
	(
		global_statement
		{
			if($global_statement.n != null) {
				tmpp.Add($global_statement.n);
			}
		}
	)* 
	NEWLINE? EOF
	{
		$n = (IHSNode)tmpp;
	}
	;

global_statement returns [IHSNode n]
	:	global_var_stmt			{ $n = $global_var_stmt.n; }
	|	func_stmt			{ $n = $func_stmt.n; }
	|	NEWLINE				{ $n = null; }
	;

// func
parameters returns [IHSNode n]
	:	p1=IDENT	{ HSParameters paras = new HSParameters(); paras.Add($p1.Text); }
		(',' p2=IDENT { paras.Add($p2.Text); }
		)*
		{ $n = (IHSNode)paras; }
	;
	
global_var_stmt returns [IHSNode n]
	:	'var' IDENT NEWLINE					{ $n = new HSVarDeclare($IDENT.Text); }
	;

func_stmt returns [IHSNode n]
	:	'func' IDENT 
		'('
			{
				HSFunction hfunc = new HSFunction($IDENT.Text);
			}
		 	(
		 		parameters
		 		{
		 			HSParameters hparam = (HSParameters)$parameters.n;
		 			hfunc.SetParameters(hparam);
		 		}
		 	)? 
		')' 
		'{'
			(
				statement
				{
					if($statement.n != null) 
					{
						hfunc.Add($statement.n);
					}
				}
			)* 
		'}' NEWLINE
		{
			$n = (IHSNode)hfunc;
		}
	;

// statements
statement returns [IHSNode n]
	:	var_stmt			{ $n = $var_stmt.n; }
	|	assign_stmt			{ $n = $assign_stmt.n; }
	|	if_stmt				{ $n = $if_stmt.n; }
	|	while_stmt			{ $n = $while_stmt.n; }
	|	return_stmt			{ $n = $return_stmt.n; }
	|	syscall_stmt		{ $n = $syscall_stmt.n; }
	|	funccall_stmt		{ $n = $funccall_stmt.n; }
	|	NEWLINE				{ $n = null; }
	;

var_stmt returns [IHSNode n]
	:	'var' IDENT NEWLINE					{ $n = new HSVarDeclare($IDENT.Text); }
	|	'var' IDENT '=' expression NEWLINE	{ $n = new HSVarDeclare($IDENT.Text, $expression.n); }
	;
	
assign_stmt returns [IHSNode n]
	:	IDENT '=' expression NEWLINE		{ $n = new HSAssignment($IDENT.Text, $expression.n); }
	;
	
if_stmt returns [IHSNode n]
	:	'if' expression '{'		{ HSIf hif = new HSIf($expression.n); }
		 	(
		 		statement
		 		{
		 			if($statement.n != null)
		 			{
		 				hif.Add($statement.n);
		 			}
		 		}
		 	)* 
		'}' NEWLINE
		{
			$n = (IHSNode)hif;
		}
	;
	
while_stmt returns [IHSNode n]
	:	'while' expression '{'	{ HSWhile hwhile = new HSWhile($expression.n); }
	 		(
	 			statement
	 			{
	 				if($statement.n != null) {
	 					hwhile.Add($statement.n);
	 				}
	 			}
	 		)* 
	 	'}' NEWLINE
	 	{
			$n = (IHSNode)hwhile;
		}
	;
	
return_stmt returns [IHSNode n]
	:	'return'
		{
			HSReturn hret = new HSReturn();
		}
		(
			expression
			{
				hret.Set($expression.n);
			}
		)? NEWLINE	
		{ $n = (IHSNode)hret; }
	;
	
syscall_stmt returns [IHSNode n]
	:	syscall NEWLINE
		{
			$n = $syscall.n;
		}
	;
	
syscall returns [IHSNode n]
	:	'$ys.' IDENT
		'('
			{
				HSSysCall scall = new HSSysCall($IDENT.Text);
			}
		 	(
		 		args
		 		{
		 			HSArguments sargs = (HSArguments)$args.n;
		 			scall.SetArguments(sargs);
		 		}
		 	)?
		 ')'
		 {
		 	$n = (IHSNode)scall;
		 }
	;
	
funccall_stmt returns [IHSNode n]
	:	funccall NEWLINE
		{
			$n = $funccall.n;
		}
	;
	
funccall returns [IHSNode n]
	:	IDENT 
		'('
			{
				HSFuncCall fcall = new HSFuncCall($IDENT.Text); 
			}
			(
				args
				{
					HSArguments fargs = (HSArguments)$args.n;
					fcall.SetArguments(fargs);
				}
			)? 
		')'
		{
			$n = (IHSNode)fcall;
		}
	;
	
args returns [IHSNode n]
	:	fr=expression	
		{ 
			HSArguments arg = new HSArguments();
			arg.Add($fr.n);
		}
		(',' 
		rr=expression
		{
			arg.Add($rr.n);
		}
		)*
		{
			$n = arg;
		}
	;

// expressions
term returns [IHSNode n]
	:	IDENT					{$n = new HSVariable($IDENT.Text);}
	|	'(' expression ')'		{$n = $expression.n;}
	|	STRING					{$n = new HSString($STRING.Text);}
	|	NUMBER					{$n = new HSNumber(int.Parse($NUMBER.Text));}
	|	syscall					{$n = $syscall.n;}
	|	funccall				{$n = $funccall.n;}
	;
	
negation returns [IHSNode n]
	: { bool neg = false; }	
	  ('!'{neg = !neg;})* term
	  {
	  	if(neg == true)
	  	{
	  		$n = new HSNegation($term.n);
	  	}
	  	else
	  	{
	  		$n = $term.n;
	  	}
	  }
	;
	
unary returns [IHSNode n]
	: {bool negative = false;}	
	('+' | '-' {negative = !negative;})* negation
	{
		if(negative == true)
		{
			$n = new HSNegative($negation.n);
		}
		else
		{
			$n = $negation.n;
		}
	}
	;
	
mult returns [IHSNode n]
	:	op1=unary 			{ $n = $op1.n; }
		('*' op2=unary		{ $n = new HSMultiply($n, $op2.n); }
		|'/' op2=unary		{ $n = new HSDivide($n, $op2.n); }
		)*
	;
	
add returns [IHSNode n]
	:	op1=mult			{ $n = $op1.n; }
		('+' op2=mult		{ $n = new HSAddition($n, $op2.n); }
		|'-' op2=mult		{ $n = new HSSubtract($n, $op2.n); }
		)*
	;
	
relation returns [IHSNode n]
	:	op1=add				{ $n = $op1.n; }
		('==' op2=add		{ $n = new HSEqualTo($n, $op2.n); }
		|'!=' op2=add		{ $n = new HSNotEqualTo($n, $op2.n); }
		|'<' op2=add		{ $n = new HSLessThan($n, $op2.n); }
		|'<='op2=add		{ $n = new HSLessThanEqualTo($n, $op2.n); }
		|'>' op2=add		{ $n = new HSMoreThan($n, $op2.n); }
		|'>=' op2=add		{ $n = new HSMoreThanEqualTo($n, $op2.n); }
		)*
	;
	
expression returns [IHSNode n]
	:	op1=relation 		{ $n = $op1.n; }
		('||' op2=relation		{ $n = new HSOr($n, $op2.n); }
		|'&&' op2=relation		{ $n = new HSAnd($n, $op2.n); }
		)*
	;

IDENT	
	:	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')* ('.' ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*)*
	;

NUMBER
	:	'0'..'9'+
	;
		
STRING
	:	'"' STRING_CHAR* '"'
	;

fragment
STRING_CHAR
	:	ESC_SEQ | ~('"' | '\n')
	;
		
fragment
ESC_SEQ
	:	'\\' ('b' | 't' | 'n' | 'f' |'r' | '"' | '\\')
	;
		
NEWLINE
	:	('\r'? '\n')+
	;
	
COMMENT
	:	'#' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
	;

WS
	:	( ' ' | '\t' ) {$channel=HIDDEN;}
	;
